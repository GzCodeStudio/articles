# Articles

dhbin modified the readme.md

## 本地运行

环境：nodejs、hexo-cli

环境安装参考

[nodejs](https://nodejs.org/en/)

[hexo-cli](https://hexo.io/zh-cn/docs/)

安装如果过慢，使用淘宝源，在执行命令后面加上

```
--registry=https://registry.npm.taobao.org
```

### 初始化

```sh
npm i --registry=https://registry.npm.taobao.org
```

### 运行

```sh
hexo s
```

## 提交

文章位于**source/_posts**中，以自己的**名字**命名一个文件夹作为工作目录，模板如下：

```markdown
---
title: 标题
date: 2019-04-28 23:10:15
categories:
	- [categories]
tag:
	- tag
author: 作者
---

摘要

<!--more-->

正文
```

其中**title**、**date**、**author**必须存在，**categories**（分类）、**tag**（标签）

标签和分类的使用参考[material-x文章分类](https://xaoxuu.com/wiki/material-x/front-matter/index.html#%E6%96%87%E7%AB%A0%E5%88%86%E7%B1%BB)

更多配置参考[material-x页面配置](https://xaoxuu.com/wiki/material-x/front-matter/index.html)

## 注意

**在提交到仓库前，请先在本地运行一次，确保没问题再提交！！！**

**在提交到仓库前，请先在本地运行一次，确保没问题再提交！！！**

**在提交到仓库前，请先在本地运行一次，确保没问题再提交！！！**
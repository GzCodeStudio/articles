---
title: '[JavaScript]文档流加载完毕后使用document.write，会导致覆盖整个文档'
date: 2019-04-29 00:00:00
categories:
	- [JavaScript]
author: 陈俊鸿
---

最近学习JavaScript语法时遇到的一些疑惑，解决之后想和大家分享一下。

<!--more-->

![](http://lc-ypSPzXKt.cn-n1.lcfile.com/c794fbb9e08ada37bc01/1.jpg)





***



# 以下是我探究的例子：



```html
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>

  <p>hello world!</p>
  <script>
  document.write("2333");
  </script>  
  <p>255</p>

</body>

</html>
```

### 输出效果：

![2](http://lc-ypSPzXKt.cn-n1.lcfile.com/4009a4f65aeff6285ff3/2.jpg)





##### 了解一下document对象:

- 1.当浏览器载入HTML文档,它就会成为Document对象(也就是这个文档的高级管理员)

- 2.Document对象是HTML文档的根节点

- 3.Document对象使我们可以从脚本中对HTML页面中的所有元素进行访问

- 4.Document对象是Window对象的一部分,可以通过window.document属性对其进行访问.

 

~~~html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
```html
</head>
<body>

  <p>hello world!</p>
  <script>
  document.write("2333");
  </script>  

  <button onclick="document.write('555');">确定</button>

  <p>255</p>

</body>
</html>
~~~





***



##### 这里解释下为什么文档流加载后，使用该方法会导致内容被覆盖。

html代码在被执行的时候,浏览器会自动调用document.open()打开一个新的文档流,然后开始在这个文档流的基础上加载信息.加载完毕后,就会自动调用document.close()关闭该文档流.上述代码的按钮被触发的时候,原来一开始被加载时使用的文档流已经在文档加载完毕后自动被关闭了,如果再执行document.write(),就会自动调用document.open()来创建一个新的文档流来写入信息.两次加载的文档流并不是同一个.因此就有被"清空"的现象.

但是注意:如果js脚本并不是被触发的,是那种嵌入html文档中就能够直接被执行的,那么就不会在原文档流被关闭之后执行,他就能够使用到同一个文档流,就不会有被"清空"的现象发生.

onclick事件是在文档内容完全加载完毕再被执行事件处理函数,当文档加载完毕了,当前的文档流已经关闭了,这时候事件被触发所执行的document.write会自动去调用document.open()函数穿件一个新的文档流,并写入新的内容,再通过浏览器展示,这样就会覆盖原来的内容.也就是当文档流没有关闭之前执行是不会被清空的.文档流是由浏览器创建的,没有办法手动关闭.但是经过document.open()创建的文档流可以由document.close()关闭.

```
<!DOCTYPE html>
<html>
<head>
<meta charset=" utf-8">
<title>Document</title>
<script type="text/javascript">
    document.write("JavaScript");
</script>
</head>

<body>
    <div>Hello JavaScript</div>
</body>

</html>
```



### 输出效果：

![](http://lc-ypSPzXKt.cn-n1.lcfile.com/e82df540f5aaad069214/3.jpg)







<strong>重点在于:不要认为有document.write(),就会有"清空"的现象.会不会被清空的关键在于js执行输入流的顺序,如果在文档加载完毕了再执行,那么文档加载完毕就会把流给关闭了, 就要重新创建流,内容会被清空,js执行输入流的时,当前文档流执行过程只是暂时被中断,依旧使用当前文档流先来执行js脚本,处理完毕后,接着执行下面的内容.等到初始的文档流被自动关闭的时候,js已经执行了.于是就不会被"清空".</strong>
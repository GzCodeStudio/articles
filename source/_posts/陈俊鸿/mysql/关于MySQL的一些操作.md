---
title: 关于MySQL的一些操作
date: 2019-04-28 00:00:00
categories:
	- [mysql]
tag:
	- mysql
author: 陈俊鸿
---



# 关于MySQL的一些操作

大家好，我是小白。本学期开启了 《MySQL 数据库》这门课程，数据库可是后台操作的重要部分之一，在方方面面都得到了充分的应用。以下是我学到的一些知识点。

<!--more-->

## 1.虚拟机的相关语法：

##### 进入虚拟机：

LOGIN:root 						管理员账号

PASSWORD:root				管理员口令

hostname -I						查询ip地址

shutdown -h now			   关机

##### 进入Putty前：

+ 1.在Host Name处，输入hostname -I 得到的IP地址 ，在 Saved Sessions 那输入一个文件名，方便下次使用。
+ 2.为了方便查看数据，可以在Category（目录）进行字体大小的等设置
+ 3.字体设置为Category / Appearance / Font-settings / change 进行字体样式及大小等设置
+ 4.常规设置完毕后记得回到Category / Session ，点击 Saved Sessions 创建的文件名，进行Save保存。

##### 进入Putty后：

- 1.输入虚拟机的账号和密码（温馨提示，输入密码的时候数字键的键盘灯会熄灭，请开启后再输入，避免密码无法输入的尴尬情况）

- 2.检查MySQL服务：

  ​	systemctl status mysqld 					查询MySQL服务状态

  ​	systemctl start mysqld						开启MySQL服务

     ---------------------   以下为相关操作语法   ---------------------

  ​	systemctl enable mysqld					MySQL服务随着操作系统启动而自动启动
  ​	systemctl disable mysqld				    MySQL服务手动启动

  ​	systemctl stop mysqld						停止MySQL服务
  ​	systemctl start mysqld						开启MySQL服务
  ​	systemctl restart mysqld					重启MySQL服务

- 3.确认MySQL服务开启后，输入自己设置好的MySQL账号和密码

  ​	例：mysql -uroot -p Root@1995	-p后面接的是自己设置的账号

- 4.成功登陆之后就可以对数据库进行操作啦！

  ​	1.show databases;					显示数据库

  ​	2.create database 数据库名	创建一个数据库

  ​	3.use 数据库名;		  				需要使用的数据库（一定要记得use，否则无法MySQL服务无法辨别您的操	操作是针对于哪个库的QAQ）

  ​	4.create table 数据表名;		在数据库里创建一个数据表

  ​	5.show tables;						显示数据库里存在的数据表

- 5.以上操作完毕后就可以对里面的数据进行操作啦！Hhhha

  ​	select * from xx（表名）		提取xx表的所有数据（“ * ” 星号代表的是 所有数据 ）

  ​	desc 表名			   					描述数据表里的结构

  ​	drop table								 删除这个数据表

  ​	以上便是小白近来学到的一些关于MySQL数据库的一些知识点，如有错误之处还望各位指出，非常开心能	够得到各位大佬的指教。谢谢！

  ​	

  

  
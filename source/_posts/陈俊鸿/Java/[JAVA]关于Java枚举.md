---
title: '[JAVA]关于Java枚举'
date: 2019-05-01 15:10:00
categories:
	- [java]
author: 陈俊鸿
---

<strong><font color=red size=4>枚举是一个被命名的整型常数的集合，用于声明一组带标识符的常数。</font><Font size=3>枚举在曰常生活中很常见，例如一个人的性别只能是“男”或者“女”，一周的星期只能是 7 天中的一个等。类似这种当一个变量有几种固定可能的取值时，就可以将它定义为枚举类型。 </Font></strong>

<!--more-->

枚举类型与普同常量相比，具有<strong>简洁性</strong>、<strong>安全性</strong>以及 <strong>便捷性</strong>

### 定义常量：

创建枚举类型要使用 enum 关键字，隐含了所创建的类型都是 java.lang.Enum 类的子类（java.lang.Enum 是一个抽象类）。

```java
public enum MyEnumTest1 {
    SQ("帅气", 1), CS("成熟", 2), LQ("老气", 3);
}
```



### 相关事例：

```java
/**
 * 枚举测试类
 * 
 */
public enum MyEnumTest1 {
	SQ("帅气", 1), CS("成熟", 2), LQ("老气", 3);

	private String name;
	private int index;

// 其中构造方法的访问权限必须比类的权限小
	private MyEnumTest1(String name, int index) {
		this.name = name;
		this.index = index;
	}

	public static String getName(int index) {
		for (MyEnumTest1 c : MyEnumTest1.values()) {
			if (c.getIndex() == index) {
				return c.getName();
			}
		}
		return null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	//主函数
	public static void main(String[] args) {
		for (int i = 1; i <= MyEnumTest1.values().length; i++) {
			System.out.println("第" + i + "个元素是：" + MyEnumTest1.getName(i));
		}
	}
}

```



### 注意：

<Font color=red >枚举enum默认是继承自java.lang.Enum类的，在以上代码中，MyEnumTest1为一个枚举类， SQ, CS, LQ为这个enum类的实例，由于MyEnumTest1已经继承自java.lang.Enum类，所以它是不能在继承其它enum类的!</Font>



### 以下是关于枚举的常用方法：

|  方法名称   |               描述               |
| :---------: | :------------------------------: |
|  values()   | 以数组形式返回枚举类型的所有成员 |
|  valueOf()  |    将普通字符串转换为枚举实例    |
| compareTo() |  比较两个枚举成员在定义时的顺序  |
|  ordinal()  |      获取枚举成员的索引位置      |


---
title: '[JAVA]关于Java接口的通俗解释'
date: 2019-04-30 00:10:00
categories:
	- [java]
author: 陈俊鸿
---



<strong>日常生活中，两个实体之间进行连接的部分称为接口。如电脑和U盘连接的标准USB接口。接口可以确保不同实体之间的顺利连接。如不同的电脑厂家和U盘厂家只要按照相同的USB接口进行生产，那么所有的电脑和U盘就可以顺利的连接起来。</strong>





<!-- more -->

---





### （1）Java编程领域中，**接口可以为不同类顺利交互提供标准**。

例如：老师让学生张三和李四一起完成，java程序来模拟营业员和计算器的行为。张三和李四进行工作分工，张三写计算机器，李四写营业员类。

张三和李四没有定义接口会出现的问题如下：

张三先定义了一个计算器类Calculator类并提供计算的方法,注方法的名称

```java
public class Calculator{

public double count(doublesalary,double bonus){

return salary + bonus;

}

}

//李四定义了代表营业员的Seller类：注意这里计算器的方法

class Seller{

String name;//营业员的名称

Calculator calculator;

public Seller(String name, Calculatorcalculator) {

super();

this.name = name;

this.calculator = calculator;

}

//计算的方法

public void quote(double salary,double bonus){

System.out.println(name+"说：您好：请支付"+calculator.countMoney(salary,bonus)+"元。");

}

}
```

我们看到李四开发的时候想使用张三已经写好的计算器的类，李四想当然的认为计算钱的方法是countMoney,但是张三写计算器的时候使用的方法是count,那么李四的写的Seller类是错误的。

实现接口的好处如下：

为了保证张三和李四的书写的类可以正确的完成交互，李四定义了一个接口，并要求张三必须实现这个接口，接口的代码如下：

```java
interface Icount{

public double countMoney(double salary,double bonus);

}

//那么张三在写计算器Calculator；类的时候，实现Icount接口必须重写接口中的抽象方法。那么张三的代买就应该是如下的：

public class Calculator implemenetsIcount{

public double countMoney(double salary,double bonus){

return salary+bonus;

}

}
```

这样就李四的代码就可以正常的执行了。

### （2）接口可以降低类的依赖性，提高彼此的独立性

张三现在觉得计算器类的名字改成SuperCalculator更合适那么张三写的类代码就应该如下：

```java
public class SuperCalculator implements Icount{

public double countMoney(double salary,double bonus){

return salary+bonus;

}

}

//李四的代码如下注意这里计算的使用接口来声明：

class Seller{

String name;//营业员的名称

Icount calculator; //这里使用接口声明

public Seller(String name, Calculatorcalculator) {

super();

this.name = name;

this.calculator = calculator;

}

//计算的方法

public void quote(double salary,double bonus){

System.out.println(name+"说：您好：请支付"+calculator.countMoney(salary,bonus)+"元。");

}

}
```

由以上的代码来看张三的类无论命名成什么李四的代码都不需要改，所以代码的依赖性降低，便于程序的维护。

### （3）接口在使用的时候需要注意一下几点：

- 1) 接口是系统中最高层次的抽象类型。

- 2) 接口本身必须十分稳定，接口一旦定制，就不允许随意修改，否则对接口实现类以及接口访问都会造成影响。
---
title: '[JAVA]关于Java多态'
date: 2019-05-09 00:01:10
categories:
	- [java]
author: 陈俊鸿
---

### 多态

<!-- more -->

##### 定义：多态是同一个行为具有多个不同表现形式或形态的能力。

好处：提高了代码的维护性和扩展性。
		弊端：父类不能使用子类的特有功能。



###  示例代码：

```java
package test;

public class aboutPoly {
	public static void main(String[]args) {
System.out.println("---------------分割线--------------");
System.out.println("强制转型：");
		
	//miao类型为Animals时，AnimalsCat可转为子类cat，也可不转保持父类Animals。
	//miao类型为cat时，AnimalsCat需要强制将父类Animals转型为子类cat
	Animals AnimalsCat = new cat();			//自动向上转型
	cat miao = (cat)AnimalsCat;				//强制转型。成功的条件是这个父类AnimalsCat是经过子类cat向上塑型转换来的。
	miao.say();
	Animals miaomiao = AnimalsCat;
	miaomiao.say();

System.out.println("---------------分割线--------------");

        Animals animals = new Animals();
		animals.say();
		Animals animals1 = new cat();
		animals1.say();
		System.out.println(animals1.name);
		cat cat1 = new cat();
		cat1.say();
		System.out.println(cat1.name);
		System.out.println("animals1和cat1调用的成员方法是一样的");
		
System.out.println("---------------分割线--------------");

	System.out.println("用instanceof函数判断其左边对象是否为其右边类的实例，相等于 ==");
	if (cat1 instanceof Animals) {
		System.out.println("是");
	}else {
		System.out.println("不是");
	}

System.out.println("---------------分割线--------------");
	}
}

//父类
class Animals{
	String name = "动物祖先";
	public void say(){
		System.out.println("发声...");
	}
}
//子类
class cat extends Animals {
	String name = "我是猫猫";
	public void say() {
		System.out.println("喵喵喵...");
	}

    public void run() {
        System.out.println(name + "跑啊！");
    }
}

class dog extends Animals{
	String name = "我是狗狗";
	public void say() {
		System.out.println("汪汪汪...");
	}

    public void run() {
        System.out.println(name + "跑啊！");
	}
}
```

### 运行结果：

```java
---------------分割线--------------
强制转型：
喵喵喵...
喵喵喵...
---------------分割线--------------
发声...
喵喵喵...
动物祖先
喵喵喵...
我是猫猫
animals1和cat1调用的成员方法是一样的
---------------分割线--------------
用instanceof函数判断其左边对象是否为其右边类的实例，相等于 ==
是
---------------分割线--------------
```

### 注意要点：

- 对于存在继承关系的强制类型转换：

- 子类转换为父类属于向上塑型，可以直接转换
- 父类转换为子类属于向下塑型，需要强制类型转换，但是不一定成功。成功的条件是这个父类是经过子类向上塑型转换来的
- 对于不存在继承关系的强制类型转换，一般都是失败的（如果不写转换方法的话）
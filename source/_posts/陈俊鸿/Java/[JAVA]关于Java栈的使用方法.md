---
title: '[JAVA]关于Java栈的使用方法'
date: 2019-05-04 15:20:00
categories:
	- [java]
author: 陈俊鸿
---

### 最近学习了有关<stong>栈</strong>的一些知识，以下是学习笔记，后续还会有 补充。



<!--more-->



## 关于栈

​			栈（Stack）是限定只能在一段进行插入和删除操作的线性表。

　　进行插入和删除操作的一端称为“栈顶”（top），另一端称为“栈底”（bottom）。

　　栈的插入操作称为“入栈”(push)，栈的删除 操作称为“出栈”（pop）。

　　栈具有后进先出（LIFO），先进后出(FILO)的特性。



## Java Stack 类　

　　栈是Vector的一个子类，它实现了一个标准的后进先出的栈。

　　堆栈只定义了默认构造函数，用来创建一个空栈。 堆栈除了包括由Vector定义的所有方法，也定义了自己的一些方法。





### 代码示例：

```java
import java.util.Stack;
/**

- 栈放入的是对象，所以需要new Stack<Integer>。

- @author 陈俊鸿
  *
  */
  public class stack {

  public static void main(String[] args) {
  	Stack<Integer> sta = new Stack<Integer>();
  	//push()	把元素压入堆栈顶部。
  	sta.push(5);
  	sta.push(4);
  	sta.push(3);
  	//再次添加一个元素进入栈顶，如果用赋值的方式添加，用要注意数据类型！
  	Object add = sta.push(666);
  	System.out.println("PUSH方法添加进来的值:  " + add);
  	System.out.println("完整栈：" + sta);
  	System.out.println();

  	//pop()		移除堆栈顶部的对象，并作为此函数的值返回该对象。
	//每次pop都会从栈顶移除一个对象
	sta.pop();
	System.out.println("POP方法后的完整栈：" + sta);
	System.out.println();
		
	//peek()	查看堆栈顶部的对象，但不从堆栈中移除它。
	System.out.println("PEEK方法：  " + sta.peek());
	System.out.println("PEEK方法后的完整栈：" + sta);
	System.out.println();

	//isEmpty()	判断堆栈是否为空。
	boolean judge = sta.isEmpty();
	System.out.println("isEmpty判断栈是否为空：  " + judge);
	System.out.println();
		
	//search()	返回对象在堆栈中的位置，以 1 为基数。
	int index = sta.search(3);
	System.out.println("search返回对象在堆栈中的位置：  " + index);		
		
	}

}

```

### 输出结果：

![](http://lc-ypSPzXKt.cn-n1.lcfile.com/ae632d82327b7a7fd158/1556954271%281%29.jpg)


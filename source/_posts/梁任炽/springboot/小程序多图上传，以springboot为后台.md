---
title: 小程序多图上传，以springboot为后台
date: 2019-04-29 
categories:
	- [JAVA]
author: 梁任炽
---



小程序上传接囗

<!--more-->

```javascript
 //上传图片
  submit_image(e){
    var that = this;
    console.log(that.data.fileList)
    //将图片路径循环赋值给filePath参数
    for (var i = 0; i < that.data.fileList.length; i++) {
      var imgUrl = that.data.fileList[i].url;
      console.log(imgUrl)
      wx.uploadFile({
        //上传图片的网路请求地址
        url: 'http://localhost:8080/upload',
        //选择
        filePath: imgUrl,
        name: 'file',
        success: function (res) {
          console.log("success");
        },
        fail: function (res) {
          console.log("error");
        }
      });
    }//for循环结束
  }
```



springboot后台实现

> 文件上传	

在全局配置文件application,properties中添加如下配置

```properties
#单个文件大小
spring.http.multipart.max-file-size=500Mb
#设置总上传的数据大小
spring.http.multipart.max-request-size=500Mb
```

在springboot项目中添加webapp目录用来存放图片资源，具体步骤如下图:

![1556540807716](http://bmob-cdn-8289.b0.upaiyun.com/2019/04/29/7dd95c2a40abc1b980c26ca4631c6e8f.png)



文件上传与显示:

```java
package com.second.controller;


import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.UUID;

@Controller
public class UploadController {

    private ResourceLoader resourceLoader;

    /**
     * 图片上传
     * @param file
     * @param request
     * @return
     */
    @RequestMapping(value = "/upload",method = RequestMethod.POST)
    @ResponseBody
    public String uploadFile(MultipartFile[] file, HttpServletRequest request){
        try{
            for (int i=0;i<file.length;i++){
                //创建文件在服务器端的存放路径
                String dir=request.getServletContext().getRealPath("/upload");
                File fileDir = new File(dir);
                if (!fileDir.exists())
                    fileDir.mkdir();
                //生成文件在服务器端存放的名字
                String fileSuffix = file[i].getOriginalFilename().substring(file[i].getOriginalFilename().lastIndexOf("."));
                String fileName = UUID.randomUUID().toString()+fileSuffix;

                File files = new File(fileDir+"/"+fileName);

                //上传
                file[i].transferTo(files);
            }

        }catch (Exception e){
            e.printStackTrace();
            return "上传失败";
        }

        return "上传成功";
    }

    /**
     * 图片显示
     * @param fileName
     * @param request
     * @return
     */
    @RequestMapping("/{fileName:.+}")
    @ResponseBody
    public ResponseEntity show(@PathVariable String fileName, HttpServletRequest request){
        try {
            //resourceLoader.getResource("file:" + uploadPicturePath + fileName) 返回指定路径的资源句柄，这里返回的就是URL [file:D:/springboot/upload/test.png]
            //ResponseEntity.ok(T) 返回指定内容主体
            return ResponseEntity.ok(resourceLoader.getResource("file:"+request.getServletContext().getRealPath("/upload")+fileName));
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

}

```








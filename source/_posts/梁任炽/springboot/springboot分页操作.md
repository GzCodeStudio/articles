---
title: springboot分页操作
date: 2019-4-29
categories:
	- [小程序]
author: 梁任炽

---

在pom.xml中添加如下依赖

<!--more-->

```xml
   <dependency>
            <groupId>com.github.pagehelper </groupId>
            <artifactId>pagehelper</artifactId>
            <version>4.1.0</version>
        </dependency>
```

在springboot的启动类（WarApplication.java)里面注入:

```java
package com.second.config;

import com.github.pagehelper.PageHelper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
public class MybatisConfiguration {
    @Bean
    public PageHelper pageHelper(){
        PageHelper pageHelper = new PageHelper();
        Properties p = new Properties();
        p.setProperty("offsetAsPageNum", "true");
        p.setProperty("rowBoundsWithCount", "true");
        pageHelper.setProperties(p);
        return pageHelper;
    }

}

```

Controller分页写法

```java
 /**
     * 査找某人发布信息
     */
    @RequestMapping(value = "/findGoodByStudent",method = RequestMethod.GET)
    public ResultModel findGoodByStudent(Integer student_id,Integer page){
        //startPage(第几页,每页显示的条数)
        PageHelper.startPage(page,3);
        return goodService.findGoodByStudent(student_id);
    }
```

效果:

![微信图片_20190429122332](/image/微信图片_20190429122332.png)










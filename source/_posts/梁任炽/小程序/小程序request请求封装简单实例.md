---
title: 小程序request请求封装简单实例
date: 2019-04-27 00:00:00
categories:
	- [小程序]
author: 梁任炽
---



在utils文件夹下新建request.js文件，代码如下

<!--more-->

```javascript
/**
 * url:请求的url
 * params:请求参数
 * message:loading提示信息
 * success:成功的回调
 * fail:失败的回调
 */

//post请求
function postRequest(url, params, success, fail) {
  this.postRequestLoading(url, params, "", success, fail)
}


//根据判断message 是否显示loading
function postRequestLoading(url, params, message, success, fail) {
  if (message != "") {
    wx.showLoading({
      title: message,
    })
  }
  const postRequestTask = wx.request({
    url: url,
    data: params,
    header: {
      'content-type': 'application/x-www-form-urlencoded'
    },
    method: 'POST',
    success: function (res) {
      if (message != "") {
        wx.hideLoading()
      }
      if (res.statusCode == 200) {
        success(res.data)
      } else {
        fail(res)
      }
    },
    fail: function (res) {
      if (message != "") {
        wx.hideLoading()
      }
      fail(res)
    }
  })
}




//get请求
function getRequest(url, params, success, fail) {
  this.getRequestLoading(url, params, "", success, fail)
}

function getRequestLoading(url, params, message, success, fail) {
  if (message != "") {
    wx.showLoading({
      title: message,
    })
  }
  const getRequestTask = wx.request({
    url:  url,
    data: params,
    header: {
      'Content-Type': 'application/json'
    },
    method: 'GET',
    success: function (res) {
      if (message != "") {
        wx.hideLoading()
      }
      if (res.statusCode == 200) {
        success(res.data)
      } else {
        fail(res)
      }
    },
    fail: function (res) {
      if (message != "") {
        wx.hideLoading()
      }
      fail(res)
    }
  })
}

//取消post请求
function abortPostRequest(url, params, success, fail) {
  postRequestTask.abort()
}

//取消get请求
function abortGetRequest(url, params, success, fail) {
  getRequestTask.abort()
}

module.exports = {
  postRequest: postRequest,
  postRequestLoading: postRequestLoading,
  getRequest: getRequest,
  getRequestLoading: getRequestLoading,
  abortPostRequest: abortPostRequest,
  abortGetRequest: abortGetRequest
}
```



使用: 在需要使用模块对应的js引入request.js文件

```javascript
var network = require('../../../utils/request.js')
```

在对应的请求函数使用接囗

```javascript
 network.postRequest(url,params,
      function(data){
        console.log("请求成功")
      },function(data){
        console.log("请求失败")
      }
    )
```


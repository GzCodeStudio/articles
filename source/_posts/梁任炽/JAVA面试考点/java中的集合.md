---
title: java中的集合
date: 2019-5-10  19:10:30
categories:
	- [JAVA面试考点]
tag:
        - JAVA
author: 梁任炽
---



java中的集合分为value，key->value(Collection Map)两种

<!--more-->

存储值分为list和set

​	List是有序的，可以重复的

​	Set是无序的，不可以重复的。根据equals和hashcode判断，也就是如果一个对象要存储在Set中,必须重写equals和hashcode方法

存储key->value的为map


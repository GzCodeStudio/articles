---
title: 有了基本的数据类型，为什么还需要包装类型
date: 2019-05-10 09:20:00
categories:
	- [JAVA面试考点]
tag:
    - JAVA
author: 梁任炽
---

我们都知道在Java语言中，new一个对象存储在堆里，我们通过栈中的引用来使用这些对象；但是对于经常用到的一系列类型如int，如果我们用new将其存储在堆里就不是很有效——特别是简单的小的变量。所以就出现了基本类型，同C++一样，Java采用了相似的做法，对于这些类型不是用new关键字来创建，而是直接将变量的值存储在栈中，因此更加高效。

<!--more-->

基本数据类型，java中提供了8种基本的数据类型。如boolean   int  float等.



包装类型：每一个基本的数据类型都会一一对应一个包装类型.

boolean->Boolean

int-->Integer

.......



#### 要弄清这个问题首先得弄清楚两个概念:`装箱和拆箱`   

装箱:把基本的数据类型转化成对应的包装类型。

Integer i = 1;   自动装箱，实际上在编译会调用Integer.valueOf方法来装箱

```java
   public static Integer valueOf(int i) {
        if (i >= IntegerCache.low && i <= IntegerCache.high)
            return IntegerCache.cache[i + (-IntegerCache.low)];
        return new Integer(i);
    }
```



拆箱:把基本的包装类型转化成对应的数据类型。

```java
Integer i = 1;
int j = i;//自动拆箱  //int j = i.intValue();  手动折箱
```

自动拆箱:实际编译调用intValue()方法



#### **二者的区别**

1. 声明方式不同：基本类型不使用new关键字，而包装类型需要使用new关键字来在堆中分配存储空间；

2. 存储方式及位置不同：基本类型是直接将变量值存储在栈中，而包装类型是将对象放在堆中，然后通过引用来使用；

3. 初始值不同：基本类型的初始值如int为0，boolean为false，而包装类型的初始值为null；

4. 使用方式不同：基本类型直接赋值直接使用就好，而包装类型在集合如Collection、Map时会使用到

   

#### 总结原因:

java是一个面向对象的语言，而基本的数据类型不具备面向对象的特性.

null Integer -->null int --->0 用Integer和int 分别表示Person这个类的ID

max  最大值	

min	最小值

缓存值:对象缓存，Integer i = 1;Integer j=1;i==j;

比如果业务中的一条数据，通过id去判断时候存在数据库的时候，包装类的好处就出来了，可以用Integer 来表示存储ID,判断它是否为空即可








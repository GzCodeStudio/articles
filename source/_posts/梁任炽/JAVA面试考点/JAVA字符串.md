---
title: JAVA字符串
date: '2019/5/10 10:26'
categories:
  - [JAVA面试考点]
tags:
  - JAVA
author: 梁任炽
---



讲一下String和StringBuilder的区别(final)？StringBuffer和StringBuilder的区别?

<!--more-->

1.在java中提供三个类String, StringBuffer,StringBuilder来表示和操作字符串，字符串就是多个字符的集合。

String是内容不可变的字符串，String底层使用了一个不可变的字符数组(final char[])

String str= new String("bbb");

```java
private final char value[];
```

而StringBuilder，StringBuffer是内容可变的字符串。底层使用的是可变的字符数组(没有使用final来修饰)

```java
char[] value;
```

2.最经典就是拼接字符串:

1. String进行拼接 String c = "a"+"b";
2. StringBuilder或StringBuffer StringBuilder sb = new StringBuilder();sb.append("a").append("b")



拼接字符串不能使用String进行拼接，要使用StringBuilder或StringBuffer

3.StringBuilder是线程不安全的，效率较高。而StringBuffer是线程安全，效率较低。




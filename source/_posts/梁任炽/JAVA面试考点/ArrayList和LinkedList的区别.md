---
title: ArrayList和LinkedList的区别
date: 2019-5-10 19:32:10
categories:
	- [JAVA面试考点]
tag:
        - JAVA
author: 梁任炽

---

List常用的ArrayList和LinkedList，区别和使用场㬌?

<!--more-->

ArrayList底层使用的是数组。LinkedList使用的是链表.

数组查询具有所有查询特定元素比较快，而插入、删除、修改比较慢（数组在内存中是一块连续的内存，如果插入或删除是需要移动内存）。

链表不要求是连续的，在当前元素中存放下一个或上一个元素地址。查询时需要

从头部开始，一个一个的找，所以查询效率低。插入时不需要移动内存，只需要

引用指向即可，所以插入或者删除的效率高。



使用场㬌:

ArrayList使用查询比较多，但是插入和删除比较少的情况。而LinkedList使用在查询少而插入和删除比较多的情况。
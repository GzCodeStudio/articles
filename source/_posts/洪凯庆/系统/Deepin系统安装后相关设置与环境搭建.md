---
title: Deepin系统安装后相关设置与环境搭建
date: 2019-05-01 12:44:00
categories:
	- [系统]
tag:
	- deepin
author: 洪凯庆
---

Deepin系统安装后相关设置与环境搭建

<!--more-->



### 系统环境设置

- ##### 设置 `swap(交换分区)` 的大小

  > 1、使用 `free` 命令查看交换分区的大小，若为 0；则进行创建设置
  >
  > ![查看交换分区的大小](https://img-blog.csdn.net/20181021161624336?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
  >
  > 2、使用 `dd` 命令在自定义路径下创建 **swap文件**
  >
  > ```sh
  > # of 指定自定义文件的位置
  > # bs 指定创建分区大小时的单位，此处为：MB
  > # count 指定
  > sudo dd if=/dev/zero of=/swap bs=1M count=10240
  > ```
  > 
  >![创建swap文件](https://img-blog.csdn.net/20181021161727987?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
  > 
  >3、使用 `mkswap` 命令来格式化刚才创建的文件为 swap 格式
  > 
  >```sh
  > sudo mkswap /swap 
  > ```
  > 
  >![格式化swap文件](https://img-blog.csdn.net/20181021161809118?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
  > 
  >4、使用 `swapon` 命令来启动 **swap**
  > 
  >```sh
  > sudo swapon /swap 
  > ```
  > 
  >![启动Swap分区](https://img-blog.csdn.net/20181021161847357?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
  > 
  >5、**为防止开机失效** ，需要修改 **/etc/fstab** 文件，将新创建的交换文件添加到 fstab 文件中
  > 
  >```sh
  > # /swap 为交换分区路径
  > /swap swap swap defaults 0 0 
  > ```
  > 
  >![防止重启失效](https://img-blog.csdn.net/20181021161916582?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
  
- ##### 安装字体

  > 1、下载想要的字体文件(`*.ttf`)；此处笔者以自己编码最喜欢的 **等宽字体 Courier_New** 为例
  >
  > ![字体文件](https://img-blog.csdn.net/20181021162010519?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
  >
  > 2、使用 Deepin 系统自带的字体安装器(`深度字体安装器`)进行字体的安装
  >
  > ![字体安装](https://img-blog.csdn.net/20181021162039980?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
  >
  > 3、安装结束后，可在 `个性化 ---> 字体 ---> 等宽字体` 下查看
  >
  > ![字体查看](https://img-blog.csdn.net/20181021162108368?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
  >
  > `注意点：安装的字体文件目录为`**/usr/share/fonts/deepin-font-install**

- ##### 系统主题安装

  > 1、下载相应的主题压缩包
  >
  > 2、解压缩之后，拷贝到 **/home/用户名/.local/share/themes** 目录下
  >
  > 3、可在 `个性化 ---> 主题` 下查看， 并选择使用
  >
  > ![切换系统主题](https://img-blog.csdn.net/20181021162149285?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

### 开发环境搭建之Java

- ##### Java环境变量配置

  > 1、下载 JDK 压缩包(jdk-8u181-linux-x64.tar.gz)：[下载地址](https://www.oracle.com/technetwork/cn/java/javase/downloads/jdk8-downloads-2133151.html)
  >
  > 2、解压缩，拷贝到自定义目录
  >
  > 3、配置环境变量(**文章末尾，统一贴出**)

- ##### 出现 _JAVA_OPTIONS 问题的解决

  > 配置完环境变量之后， 会发现使用 `java -version` 会出现： **Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=gasp** 一行信息；
  >
  > ![问题展示](https://img-blog.csdn.net/20181021162236613?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
  >
  > 这是因为 `OpenJDK` 的干扰； 虽然使用上无伤大雅， 但是看起来还是有点不爽；于是干掉它
  >
  > 1、方式一：(**没试过，读者有兴趣可自行玩玩**)
  >
  > ​	将OpenJDK的相关设置拷贝到自己Jdk下的Jre中
  >
  > ```sh
  > # /etc/java-8-openjdk/properties： OpenJDK配置的位置
  > # /Diviner/DevTools/jdk1.8.0_181/jre/lib： 本地的JDK位置下的Jre
  > sudo cp -p /etc/java-8-openjdk/*.properties   /Diviner/DevTools/jdk1.8.0_181/jre/lib
  > ```
  >
  > 2、方式二：(**相对麻烦**)
  >
  > ​	将 `/etc/profile.d/java-awt-font-gasp.sh` 文件备份
  >
  > ```sh
  > sudo cp /etc/profile.d/java-awt-font-gasp.sh /etc/profile.d/java-awt-font-gasp.sh.bak
  > ```
  >
  > ​	删除 `/etc/profile.d/java-awt-font-gasp.sh` 文件即可
  >
  > ```sh
  > sudo rm /etc/profile.d/java-awt-font-gasp.sh
  > ```
  >
  > 3、方式三：(**推荐使用**)
  >
  > ​	在JDK环境变量之前添加如下语句， 方便快捷
  >
  > ```properties
  > unset _JAVA_OPTIONS
  > ```
  >
  > 注销或重启系统之后，问题解决。

### 开发环境搭建之Maven

- ##### 环境变量配置

  > 1、下载 Maven 压缩包(apache-maven-3.5.4-bin.tar.gz)：[下载地址](http://maven.apache.org/download.cgi)
  >
  > 2、解压缩，拷贝到自定义目录
  >
  > 3、配置环境变量(**文章末尾，统一贴出**)

- ##### settings.xml自定义配置

  > 1、设置本地自定义仓库位置
  >
  > ​	位置： `settings.xml` 文件在 Maven根目录下的 `conf` 文件夹下
  >
  > ​	将以下标签打开，并输入自定义仓库路径
  >
  > ```xml
  > <localRepository>/Diviner/DevTools/repo</localRepository>
  > ```
  >
  > 2、添加阿里云镜像源
  >
  > ​	在 `<mirrors>` 标签下， 添加以下代码
  >
  > ```xml
  > <mirror>
  >     <id>nexus-aliyun</id>
  >     <mirrorOf>*</mirrorOf>
  >     <name>Nexus aliyun</name>
  >     <url>http://maven.aliyun.com/nexus/content/groups/public</url>
  > </mirror>
  > ```

### 开发环境搭建之MySQL

- ##### MySQL的安装(版本：5.7)

  > 使用命令行方式安装，默认安装的为 5.7 版本；并且`没有弹出设置 root 密码的界面` ；让人极度失望
  >
  > ```sh
  > sudo apt-get update
  > sudo apt-get install mysql-server mysql-client
  > ```
  >
  > 然后一切看起来都那么的完美； 其实 坑来了；那就是安装之后登录不进去，那么接下来就开始修改密码

- ##### MySQL修改默认密码

  > 1、首先在 `/etc/my.cnf` 文件中添加如下内容
  >
  > ```xml
  > [mysqld]
  > skip-grant-tables=1
  > # 让 mysql 跳过密码，直接可以进入
  > ```
  >
  > **注意：如果 my.cnf 文件不存在，创建即可**
  >
  > 2、重启MySQL服务
  >
  > ```sh
  > systemctl restart mysql
  > ```
  >
  > 3、使用命令行进入MySQL客户端
  >
  > ```sh
  > mysql -uroot
  > ```
  >
  > 4、使用命令修改密码
  >
  > ```sh
  > # MySQL5.7之后，之前的 password 字段变为了 authentication_string 字段
  > update mysql.user set authentication_string=password('要设置的密码') where user='root' and host = 'localhost';
  > ```
  >
  > 5、修改 `my.cnf` 文件，**注释掉或者删除** 之前添加的内容： **skip-grant-tables=1**
  >
  > 6、重启MySQL服务，使用命令行进入 MySQL服务， `结果意外发生了；那就是使用刚才设置的密码依然提示认证失败`
  >
  > **7、问题的查找与解决**
  >
  > ​	①、使用 `insert` 语句向 **user** 表中新增了一个`test用户`，退出之后， 使用 `test用户` 可以登录成功； 于是和 **root ** 进行数据对比，发现了一下问题
  >
  > ​	②、查看 user 表， 发现有一个 `plugin` 字段； `root` 用户为 **auth_socket** ; 而 `test` 用户为 **mysql_native_password** ; 于是修改了 `root` 用户的值
  >
  > ![字段对比](https://img-blog.csdn.net/2018102116241260?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
  >
  > ​	修改 `plugin` 字段的值：
  >
  > ```sh
  > update mysql.user set plugin='mysql_native_password', authentication_string=password('要设置的密码') where user='root' and host = 'localhost';
  > ```
  >
  > ​	③、重新使用 `root` 登录； 登录成功；问题解决
  >
  > ![登录成功](https://img-blog.csdn.net/20181021162438451?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

- ##### MySQL修改编码为 UTF-8

  > 1、查看MySQL的默认字符编码
  >
  > ```sh
  > # 登录到MySQL命令行
  > mysql -uroot -p密码
  > # 查看默认字符编码
  > show variables like 'character%';
  > ```
  >
  > ![查看MySQL默认字符编码](https://img-blog.csdn.net/20181021162509774?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
  >
  > 2、修改 `my.cnf` 配置文件，进行字符编码修改
  >
  > ```sh
  > [mysqld]
  > # skip-grant-tables=1
  > character-set-server=utf8
  > 
  > 
  > [client]
  > default-character-set=utf8
  > 
  > 
  > [mysql]
  > default-character-set=utf8
  > ```
  >
  > ![修改字符编码](https://img-blog.csdn.net/20181021162533760?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
  >
  > 3、重启服务，再次查看字符编码是否修改成功
  >
  > ```sh
  > systemctl restart mysql
  > mysql -uroot -p
  > show variables like 'character%';
  > ```
  >
  > ![查看修改效果](https://img-blog.csdn.net/20181021162604952?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
  >
  > 至此，MySQL相关配置，已经完成

### 开发环境搭建之IDE开发工具

- ##### IntelliJ IDEA

  > 1、下载 `IntelliJ IDEA` 安装包()： [下载地址](https://www.jetbrains.com/idea/download/)
  >
  > 2、解压缩，并拷贝到自定义目录
  >
  > 3、配置应用程序快捷桌面图标
  >
  > ​	第一步： 进入到 **/usr/share/applications** 目录
  >
  > ​	第二步： 创建桌面图标 **idea.desktop** 文件
  >
  > ​	第三步： 编辑 **idea.desktop** 文件，写入以下内容
  >
  > ```shell
  > [Desktop Entry]
  > Name=Java工具
  > Exec=/Diviner/DevTools/idea/bin/idea.sh
  > Icon=/Diviner/DevTools/idea/bin/idea.svg
  > Terminal=false
  > Type=Application
  > Categories=Application;DevTool;
  > ```
  >
  > ![IDEA配置过程](https://img-blog.csdn.net/20181021162639298?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

- ##### Eclipse

  > 1、下载 `Eclipse` 安装包(eclipse-jee-2018-09-linux-gtk.tar.gz)： [下载地址](http://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2018-09/R/eclipse-jee-2018-09-linux-gtk-x86_64.tar.gz)
  >
  > 2、解压缩，并拷贝到自定义目录
  >
  > 3、配置应用程序快捷桌面图标
  >
  > ​	第一步： 进入到 **/usr/share/applications** 目录
  >
  > ​	第二步： 创建桌面图标 **idea.desktop** 文件
  >
  > ​	第三步： 编辑 **idea.desktop** 文件，写入以下内容
  >
  > ```shell
  > [Desktop Entry]
  > Name=Eclipse
  > Exec=/Diviner/DevTools/eclipse/eclipse
  > Icon=/Diviner/DevTools/eclipse/icon.xpm
  > Terminal=false
  > Type=Application
  > Categories=Application;DevTool;
  > ```
  >
  > ![Eclipse配置配置过程](https://img-blog.csdn.net/20181021162714159?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

## 附录

- ### 环境变量相关配置

  > ```shell
  > # 本地后端环境变量配置
  > unset _JAVA_OPTIONS
  > JAVA_HOME=/Diviner/DevTools/jdk1.8.0_181
  > MAVEN_HOME=/Diviner/DevTools/apache-maven-3.5.4
  > 
  > export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
  > export PATH=$JAVA_HOME/bin:$MAVEN_HOME/bin:$NODE_HOME/bin:$PATH
  > ```
  >
  > ![环境变量配置](https://img-blog.csdn.net/20181021162741126?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JhaWR1XzMyNDE5NzQ5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
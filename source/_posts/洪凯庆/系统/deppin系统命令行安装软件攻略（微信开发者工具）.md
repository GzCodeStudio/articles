---
title: deppin系统命令行安装软件攻略（微信开发者工具）
date: 2019-04-28 00:00:00
categories:
	- [系统]
tag:
	- deepin
	- 微信开发者工具
author: 洪凯庆
---



## 微信开发者工具(deepin linux 还是建议使用deppin深度商店安装此软件)

<!--more-->

一.环境:
**deepin** **linux15.9.3**
二.安装过程:

2.1 安装wine

```
sudo apt-get install wine
```

2.2 安装nwjs-sdk

2.2.1 下载linux版nwjs-sdk

```
wget https://dl.nwjs.io/v0.25.4/nwjs-sdk-v0.25.4-linux-x64.tar.gz  
```

2.2.2 解压nwjs-sdk

```
tar xvf  nwjs-sdk-v0.25.4-linux-x64.tar.gz
```

2.2.3 切换到nwjs-sdk对应的目录

```
cd nwjs-sdk-v0.25.4-linux-x64
```

2.2.3 启动nwjs-sdk

```
./nw  
```

(测试是否能正常运行,测试正常就关掉)

2.3 安装微信开发工具包(基于nwjs-sdk)

2.3.1 获取微信开发工具包

```
git clone https://github.com/cytle/wechat_web_devtools.git
```

2.3.2 切换到wechat_web_devtools目录

```
cd wechat_web_devtools
```

2.3.3 复制微信开发工具包（package.nw在wechat_web_devtools目录下）到nwjs-sdk目录下

```
cp package.nw ~/development/nwjs-sdk-v0.25.4-linux-x64/ -rf  
```

红色区域路径替换为自己的路径就行。

2.3.4 启动

```
./nw
```

我们可以最后再做一个批处理命令，用于方便启动微信开发者工具程序，具体步骤如下： 

```
touch wechat-tools.sh

gedit wechat-tools.sh
```

文件内容：

```
cd /
cd home/tianhe/nwjs-sdk-v0.25.4-linux-x64/
./nw
```

红色区域的路径替换为自己的路径就行！

使用：

```
sh wechat-tools.sh
```

 即可启动。
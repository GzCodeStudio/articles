---
title: Deppin系统软件打开备注
date: 2019-04-28 00:00:00
categories:
	- [系统]
tag:
	- deepin
author: 洪凯庆
---



#  *Deppin系统软件打开备注*

<!--more-->

## 1.Android Studio打开方式；

命令行进入：

```git
$ cd /home/hkq/opt/android-studio/bin
$./studio.sh
```

打开AndroidStudio自带的虚拟机：
点击AndroidStudio界面的左下角的Terminal
输入

```
$： sudo chown username -R /dev/kvm
```


注意：username是你开机用户名

## 2.Xftp替代工具FileZilla3打开方式：

命令行进入：

```
$ cd /home/hkq/opt/FileZilla3/bin
$./filezilla
```

## 3.Vmware虚拟机密链激活方式：(大概2019.9.09需要再次更新)

windows7企业版产品密钥kms安装密钥

Windows 7 Enterprise Volume:GVLK

Windows 7 企业版：33PXH-7Y6KF-2VJC9-XBBR8-HVTHH

Windows 7 企业版 N：YDRBP-3D83W-TY26F-D46B2-XCKRJ

Windows 7 企业版 E：C29WB-22CC8-VJ326-GHFJW-H9DH4
（1）、打开点击开始图标打开开始菜单，在搜索框输入cmd，右键cmd，选择“以管理员身份运行”；
（2）打开命令提示符，依次执行下面的代码，分别表示：安装win7企业版密钥，设置kms服务器，激活win7企业版，查询激活期限，kms一般是180天，到期后再次激活。可用的kms激活服务器有哪些。

```
slmgr /ipk 33PXH-7Y6KF-2VJC9-XBBR8-HVTHH

slmgr /skms zh.us.to

slmgr /ato

slmgr /xpr
```

## 5.远程连接腾讯云云服务器打开方式：

因为deppin系统自带了ssh软件
命令行打开：

```
~$ sudo apt-get install openssh-client 
$ ssh root@123.207.25.57
```


然后会提示输入密码的，输入即可

```
$ ssh root@123.207.25.57ssh root@123.207.25.57
```



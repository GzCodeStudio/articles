---
title: HTML考察课期末考点总结
date: 2019-06-27 21:50:00
categories:
	- [文档]
tag:
	- 考点总结
author: 洪凯庆
---

HTML考察课期末考点总结

<!--more-->

不知不觉就到期末了，明天就要考试了，根据老师发的考点知识做一下总结吧

h5课程考察重点：
-h5网页开发环境相关知识与操作要点
-js，h5，css三者关系
-常用文本设计的标签的使用方式
-列表的使用
-辨别html标签的父子关系
-css常用的属性及其常用值，及其UI效果
-css基础选择器及常用复合选择器
-js基础语法，document，date对象常用方法的使用方法
-form与input标签常用属性及使用的方法
-盒子模型
-外部资源引用的路径问题
-页面布局相关知识，display,float,position,清除浮动的方法

## **Js，h5，css三者关系:**

网页主要由三部分组成： 结构（ Structure） 、 表现（ Presentation） 和行为（ Behavior）
    HTML —— 结构， 决定网页的结构和内容（ “是什么”）
    CSS —— 表现（ 样式） ， 设定网页的表现样式（ “什么样子”）
    JavaScript（ JS） —— 行为， 控制网页的行为（ “做什么”）

## **常用文本设计的标签的使用方式:**

1.<Strong> </Strong>表强调 

    表示强调标签里面的内容，以加粗的格式显现。

2.<em></em>表示语气强调以倾斜的方式显现

    例如：敲掉澳大利亚草原上有食肉动物出没，非常危险
    
                <strong>
    
                            非洲大草原上有肉食动物出没。非常危险
    
                </strong><br>
    
            	今天天气<em>真好</em>啊
3 . <i>表倾斜 没有任何语义

4.< b>加粗 没有任何语义

5.<small> </small>小字显示。例如免责声明呀 。一些不想被客户看到的信息，就故意写小点

6.<cite>表示引用

7.<sub> 让字体变小 显示在下方

    H<sup>2</sup>o

8.<sup>让字体变小显示在上方。

9.<ins>表示插入的字符。表示方式 带下划线。

10.<del> 在字符上带一条删除线。 一般用于价格  的打折 把原价划掉  下面出来一个新价格

11.<code>表示此段是程序代码

12<pre> 预格式化。就是你敲在源码里是什么格式。在页面上就显示什么格式。一般跟code配合使用。

## **-列表的使用**:

## 无序列表

无序列表是一个项目的列表，此列项目使用粗体圆点（典型的小黑圆圈）进行标记。

无序列表始于 <ul> 标签。每个列表项始于 <li>。

```
<ul>
<li>Coffee</li>
<li>Milk</li>
</ul>
```

浏览器显示如下：

- Coffee
- Milk

## 有序列表

同样，有序列表也是一列项目，列表项目使用数字进行标记。

有序列表始于 <ol> 标签。每个列表项始于 <li> 标签。

```
<ol>
<li>Coffee</li>
<li>Milk</li>
</ol>
```

浏览器显示如下：

1. Coffee
2. Milk

## 辨别html标签的父子关系:

标签的相互关系就分为两种：

\1. 嵌套关系（父子关系）

```
<html>
    <head>
    </head>
</html>
```

 

\2. 并列关系

```
<head></head>
<body></body>
```

视频讲解：<http://www.iqiyi.com/w_19rtsqbj7x.html>

## css常用的属性及其常用值，及其UI效果：

#### 一、注释：

​        CSS注释以 "/*" 开始, 以 "*/" 结束    
​        eg:    /*p{color:red;text-align:center;}*/

#### 二、插入css样式表的方式？link 和@import 的区别是?

1.外部样式：使用  标签链接到样式表。  标签在（文档的）头部
    eg : <link rel="stylesheet" type="text/css" href="index.css">
2.内部样式表: 
    <head>
        <style>
            hr {color:sienna;}
            p {margin-left:20px;}
        </style>
    </head>
3.内联样式表：

    <p style="color:sienna;margin-left:20px">这是一个段落。

4.导入(@import)

> link和@import的区别：
>
> link是XHTML标签，除了加载CSS外，还可以定义RSS等其他事务；@import属于CSS范畴，只能加载CSS。link引用CSS时，在页面载入时同时加载；
> @import需要页面网页完全载入以后加载。link是XHTML标签，无兼容问题；@import是在CSS2.1提出的，低版本的浏览器不支持。
> link支持使用Javascript控制DOM去改变样式；而@import不支持。



**一般情况下，三种的优先级如下：（内联样式） > （内部样式） >（外部样式） > 浏览器默认样式**

#### **三、CSS 背景**

1.background-color： 背景颜色  
    CSS中，颜色值通常以以下方式定义:
        十六进制 - 如："#ff0000"
        RGB - 如："rgb(255,0,0)"
        颜色名称 - 如："red"；
     其它属性值：   
         inherit指定背景颜色，应该从父元素继承 
         transparent（透明）
        如： body {background-color:#b0c4de;}

2.background-image：背景图像
      默认情况下，背景图像进行平铺重复显示，以覆盖整个元素实体.
      如:body {background-image:url('a.jpg');}

3.background-repeat:设置背景图像是否及如何重复。
    属性值：
        repeat  背景图像将向垂直和水平方向重复。这是默认
        repeat-x  只有水平位置会重复背景图像
        repeat-y  只有垂直位置会重复背景图像
        no-repeat  背景图片不会重复
        inherit  指定背景图片属性设置应该从父元素继承

4.background-attachment：背景图像是否固定或者随着页面的其余部分滚动。    
    属性值：
        scroll  背景图片随页面的其余部分滚动。这是默认
        fixed  背景图像是固定的
        inherit  指定background-attachment的设置应该从父元素继承

5.background-position：设置背景图像的起始位置。
    属性值一般设置：left top bottom right     *x%*    0px  

#### 四、CSS 文本格式

\1.  color：文本颜色：
    十六进制值 - 如: **＃FF0000**    一个RGB值 - 如: **RGB(255,0,0)**    颜色的名称 - 如: **red**

2.text-align：文本的对齐方式
    left   把文本排列到左边。默认值：由浏览器决定。
    right  把文本排列到右边。
    center  把文本排列到中间。
    justify  实现两端对齐文本效果。

3.text-decoration:文本修饰，主要是用来删除链接的下划线
    none  默认。定义标准的文本。
    underline  定义文本下的一条线。
    overline  定义文本上的一条线。
    line-through  定义穿过文本下的一条线。

4.text-transform: 文本转大小写（控制元素中的字母）
    none  默认。定义带有小写字母和大写字母的标准的文本。
    capitalize  文本中的每个单词以大写字母开头。
    uppercase  定义仅有大写字母。
    lowercase  定义仅有小写字母。

5.text-indent： 文本缩进属性是用来指定文本的第一行的缩进。    
    如：p {text-indent:50px;}

6.direction: 设置文本方向。
    ltr默认。文本方向从左到右。
    rtl文本方向从右到左。

7.letter-spacing: 设置字符间距

\8. line-height 设置行高

\9. vertical-align: 设置元素的垂直对齐
        middle把此元素放置在父元素的中部。    
        sub垂直对齐文本的下标。
        super垂直对齐文本的上标
        top把元素的顶端与行中最高元素的顶端对齐
        bottom把元素的底端与行中最低的元素的顶端对齐。
        text-bottom把元素的底端与父元素字体的底端对齐。

\10. word-spacing: 设置字间距

\11. text-shadow ： 如： text-shadow: 2px 2px #ff0000;

#### 五、CSS 文字

1.font-style： 斜体文字的字体样式属性
    font-style:normal;    正常 - 正常显示文本
    font-style:italic;        斜体 - 以斜体字显示的文字

2.font-family :  属性设置文本的字体系列。
    p{font-family:"Times New Roman", Times, serif;}

3.font-size :  属性设置文本的大小。

4.font-weight :  指定字体的粗细。

六、链接样式

​    a:link {color:#000000;}      /* 未访问链接*/
​    a:visited {color:#00FF00;}  /* 已访问链接 */
​    a:hover {color:#FF00FF;}  /* 鼠标移动到链接上 */
​    a:active {color:#0000FF;}  /* 鼠标点击时 */

七、CSS 列表(ul)

​    list-style  简写属性。用于把所有用于列表的属性设置于一个声明中
​    list-style-image  将图象设置为列表项标志。
​    list-style-position  设置列表中列表项标志的位置。
​    list-style-type  设置列表项标志的类型。
​            none无标记。
​            disc默认。标记是实心圆
​            circle标记是空心圆。
​            square标记是实心方块。

八、CSS 表格

​    border-collapse :  表格边框(常用的)
​    table{border-collapse:collapse;}
​    table,th, td{border: 1px solid black;}

九.CSS 边框属性

\1.  border 简写属性，用于把针对四个边的属性设置在一个声明。

2.border-width 用于为元素的所有边框设置宽度，或者单独地为各边边框设置宽

\3. border-style 用于设置元素所有边框的样式，或者单独地为各边设置边框样式
         **border-style:dotted solid double dashed**               **dotted**  上边框是点状
               **solid**    右边框是实线
               **double**  下边框是双线
              **dashed**  左边框是虚线

4.border-color  设置元素的所有边框中可见部分的颜色，或为 4 个边分别设置颜色。

十、CSS 尺寸

​    1.height  设置元素的高度。

​    2.line-height  设置行高。

​    3.max-height 设置元素的最大高度。

​    4.max-width  设置元素的最大宽度。

​    5.min-height  设置元素的最小高度。

​    6.min-width   设置元素的最小宽度。

​    7.width    设置元素的宽度。

十一、px，em，rem 的区别

​        **px**像素(Pixel)。绝对单位。像素 px 是相对于显示器屏幕分辨率而言的，是一个虚拟长度单位，是计算 机系统的数字化图像长度单位，如果 px 要换算成物理长度，需要指定精度 DPI。

​        **em**是相对长度单位，相对于当前对象内文本的字体尺寸。如当前对行内文本的字体尺寸未被人为设置， 则相对于浏览器的默认字体尺寸。它会继承父级元素的字体大小，因此并不是一个固定的值。

​        **rem**是 CSS3 新增的一个相对单位(root em，根 em)，使用 rem 为元素设定字体大小时，仍然是相对大小， 但相对的只是HTML根元素。

十二、position 几个属性的作用

position 的常见四个属性值: relative，absolute，fixed，static。一般都要配合"left"、“top”、“right” 以及 “bottom” 属性使用。

**static:默认位置。**在一般情况下，我们不需要特别的去声明它，但有时候遇到继承的情况，我们不愿意见到元素所继承的属性影响本身，从而可以用Position:static取消继承，即还原元素定位的默认值。设置为 static 的元素，它始终会处于页面流给予的位置(static 元素会忽略任何 top、 bottom、left 或 right 声明)。一般不常用。

**relative:相对定位。**相对定位是相对于元素默认的位置的定位，它偏移的 top，right，bottom，left 的值都以它原来的位置为基准偏移，而不管其他元素会怎么 样。注意 relative 移动后的元素在原来的位置仍占据空间。

**absolute:绝对定位。**设置为 absolute 的元素，如果它的 父容器设置了 position 属性，并且 position 的属性值为 absolute 或者 relative，那么就会依据父容器进行偏移。如果其父容器没有设置 position 属性，那么偏移是以 body 为依据。注意设置 absolute 属性的元素在标准流中不占位置。

**fixed:固定定位。**位置被设置为 fixed 的元素，可定位于相对于浏览器窗口的指定坐标。不论窗口滚动与否，元素都会留在那个位置。它始终是以 body 为依据的。 注意设置 fixed 属性的元素在标准流中不占位置。

十三、盒子模型（box-sizing）

设置CSS盒模型为标准模型或IE模型。标准模型的宽度只包括content，二IE模型包括border和paddingbox-sizing属性可以为三个值之一：

​    content-box，默认值，只计算内容的宽度，border和padding不计算入width之内

​    padding-box，padding计算入宽度内

​    border-box，border和padding计算入宽度之内

**十四、 display有哪些值？说明他们的作用?**

​        inline（默认）–内联

​        none–隐藏

​        block–块显示

​        table–表格显示

​        list-item–项目列表

​        inline-block

十五、 **margin属性和padding属性？** 

**margin：外边距；  padding：内边距；属性值可以 有四个，如下：**

​    **margin:25px 50px 75px 100px;**        上边距为25px
​        右边距为50px
​        下边距为75px
​        左边距为100px

​    **margin:25px 50px 75px;**

​        上边距为25px
​        左右边距为50px
​       下边距为75px

​    **margin:25px 50px;**

​        上下边距为25px
​         左右边距为50px

​    **margin:25px;**

​        所有的4个边距都是25px

#### 十六.CSS隐藏元素的常见的几种方式及区别

**1.  display:none**

元素在页面上将彻底消失，元素本来占有的空间就会被其他元素占有，也就是说它会导致浏览器的重排和重绘。
不会触发其点击事件

**2.visibility:hidden**

和display:none的区别在于，元素在页面消失后，其占据的空间依旧会保留着，所以它只会导致浏览器重绘而不会重排。
无法触发其点击事件
适用于那些元素隐藏后不希望页面布局会发生变化的场景

**3. opacity:0**

将元素的透明度设置为0后，在我们用户眼中，元素也是隐藏的，这算是一种隐藏元素的方法。
和visibility:hidden的一个共同点是元素隐藏后依旧占据着空间，但我们都知道，设置透明度为0后，元素只是隐身了，它依旧存在页面中。
可以触发点击事件

十七、渐进增强和优雅降级

关键的区别是他们所侧重的内容，以及这种不同造成的工作流程的差异

​    **优雅降级**观点认为应该针对那些最高级、最完善的浏览器来设计网站。

​    **渐进增强**观点则认为应关注于内容本身，优先考虑低版本。

#### 十八、重绘和重排

​        重绘：当盒子的位置、大小以及其他属性，例如颜色、字体大小等都确定下来之后，浏览器便把这些原色都按照各自的特性绘制一遍，将内容呈现在页面上。重绘是指一个元素外观的改变所触发的浏览器行为，浏览器会根据元素的新属性重新绘制，使元素呈现新的外观。触发重绘的条件：改变元素外观属性。如：color，background-color等。注意：table及其内部元素可能需要多次计算才能确定好其在渲染树中节点的属性值，比同等元素要多花两倍时间，这就是我们尽量避免使用table布局页面的原因之一。

​        重排：当渲染树中的一部分(或全部)因为元素的规模尺寸，布局，隐藏等改变而需要重新构建, 这就称为回流。每个页面至少需要一次回流，就是在页面第一次加载的时候。

​        重绘和重排的关系：在回流的时候，浏览器会使渲染树中受到影响的部分失效，并重新构造这部分渲染树，完成回流后，浏览器会重新绘制受影响的部分到屏幕中，该过程称为重绘。

​        所以，**重排必定会引发重绘，但重绘不一定会引发重排。**

**十九、怎么让一个不定宽高的 DIV，垂直水平居中?**

**1.使用Flex：**只需要在父盒子设置：display: flex; justify-content: center;align-items: center;

**2.使用 CSS3 transform：**父盒子设置:position:relative
Div 设置:transform:translate(-50%，-50%);position:absolute;top:50%;left:50%;

**3.使用 display:table-cell 方法：**父盒子设置:display:table-cell;text-align:center;vertical-align:middle;
Div 设置:display:inline-block;vertical-align:middle;

二十、BFC相关知识（块级格式化上下文）

**定义**：BFC(Block formatting context)直译为"块级格式化上下文"。它是一个独立的渲染区域，只有 Block-level box 参 与， 它规定了内部的 Block-level Box 如何布局，并且与这个区域外部毫不相干。

**BFC布局规则**BFC 就是页面上的一个隔离的独立容器，容器里面的子元素不会影响到外面的元素。反之也如此。

BFC这个元素的垂直方向的边距会发生重叠，垂直方向的距离由margin决定，取最大值

BFC 的区域不会与浮动盒子重叠（清除浮动原理）。

计算 BFC 的高度时，浮动元素也参与计算。

**哪些元素会生成 BFC**

​        根元素

​        float 属性不为 none

​        position 为 absolute 或 fixed

​        display 为 inline-block， table-cell， table-caption， flex， inline-flex

​        overflow 不为 visible

二十一、浮动与清除浮动

####     1.浮动相关知识

​        **float属性的取值：**

​            left：元素向左浮动。

​            right：元素向右浮动。

​            none：默认值。元素不浮动，并会显示在其在文本中出现的位置。

​        **浮动的特性：**

​            浮动元素会从普通文档流中脱离，但浮动元素影响的不仅是自己，它会影响周围的元素对齐进行环绕。

​            不管一个元素是行内元素还是块级元素，如果被设置了浮动，那浮动元素会生成一个块级框，可以设置它的width和height，因此float常常用于制作横向配列的菜单，可以设置大小并且横向排列。

​        **浮动元素的展示在不同情况下会有不同的规则：**

​            浮动元素在浮动的时候，其margin不会超过包含块的padding。PS：如果想要元素超出，可以设置margin属性

​            如果两个元素一个向左浮动，一个向右浮动，左浮动元素的marginRight不会和右浮动元素的marginLeft相邻。

​            如果有多个浮动元素，浮动元素会按顺序排下来而不会发生重叠的现象。

​            如果有多个浮动元素，后面的元素高度不会超过前面的元素，并且不会超过包含块。

​            如果有非浮动元素和浮动元素同时存在，并且非浮动元素在前，则浮动元素不会高于非浮动元素

​            浮动元素会尽可能地向顶端对齐、向左或向右对齐

​        **重叠问题**

​            行内元素与浮动元素发生重叠，其边框，背景和内容都会显示在浮动元素之上

​            块级元素与浮动元素发生重叠时，边框和背景会显示在浮动元素之下，内容会显示在浮动元素之上

​            **clear属性**clear属性：确保当前元素的左右两侧不会有浮动元素。clear只对元素本身的布局起作用。取值：left、right、both

####     2. 父元素高度塌陷问题

​        **为什么要清除浮动，父元素高度塌陷**解决父元素高度塌陷问题：一个块级元素如果没有设置height，其height是由子元素撑开的。对子元素使用了浮动之后，子元素会脱离标准文档流，也就是说，父级元素中没有内容可以撑开其高度，这样父级元素的height就会被忽略，这就是所谓的高度塌陷。

####     3. 清除浮动的方法

​        **方法1：给父级div定义 高度**原理：给父级DIV定义固定高度（height），能解决父级DIV 无法获取高度得问题。优点：代码简洁缺点：高度被固定死了，是适合内容固定不变的模块。（不推荐使用）

​        **方法二：使用空元素，如****(.clear{clear:both})**原理：添加一对空的DIV标签，利用css的clear:both属性清除浮动，让父级DIV能够获取高度。优点：浏览器支持好缺点：多出了很多空的DIV标签，如果页面中浮动模块多的话，就会出现很多的空置DIV了，这样感觉视乎不是太令人满意。（不推荐使用）

​        **方法三：让父级div 也一并浮起来**这样做可以初步解决当前的浮动问题。但是也让父级浮动起来了，又会产生新的浮动问题。 不推荐使用

​        **方法四：父级div定义 display:table**原理：将div属性强制变成表格优点：不解缺点：会产生新的未知问题。（不推荐使用）

​        **方法五：父元素设置 overflow：hidden、auto；**原理：这个方法的关键在于触发了BFC。在IE6中还需要触发 hasLayout（zoom：1）优点：代码简介，不存在结构和语义化问题缺点：无法显示需要溢出的元素（亦不太推荐使用）

​        **方法六：父级div定义 伪类:after 和 zoom**

​    .clearfix:after{
​        content:'.';
​        display:block;
​        height:0;
​        clear:both;
​        visibility: hidden;
​    }
​    .clearfix {zoom:1;}

​        原理：IE8以上和非IE浏览器才支持:after，原理和方法2有点类似，zoom(IE转有属性)可解决ie6,ie7浮动问题优点：结构和语义化完全正确,代码量也适中，可重复利用率（建议定义公共类）缺点：代码不是非常简洁（极力推荐使用）

​        **经益求精写法**

​    .clearfix:after {
​        content:”\200B”;
​        display:block;
​        height:0;
​        clear:both;
​    }
​    .clearfix { *zoom:1; } 照顾IE6，IE7就可以了

##### **二十二、 用纯CSS创建一个三角形的原理是什么**

​    首先，需要把元素的宽度、高度设为0。然后设置边框样式。
​        width: 0;
​        height: 0;
​        border-top: 40px solid transparent;
​        border-left: 40px solid transparent;
​        border-right: 40px solid transparent;
​        border-bottom: 40px solid #ff0000;

#### **二十三、 常见的兼容性问题**

1.不同浏览器的标签默认的margin和padding不一样。*{margin:0;padding:0;}

2.IE6双边距bug：块属性标签float后，又有横行的margin情况下，在IE6显示margin比设置的大。hack：display:inline;将其转化为行内属性。

3.渐进识别的方式，从总体中逐渐排除局部。首先，巧妙的使用“9”这一标记，将IE浏览器从所有情况中分离出来。接着，再次使用“+”将IE8和IE7、IE6分离开来，这样IE8已经独立识别。
    {
        background-color:#f1ee18;/*所有识别*/
        .background-color:#00deff\9; /*IE6、7、8识别*/
        +background-color:#a200ff;/*IE6、7识别*/
        _background-color:#1e0bd1;/*IE6识别*/
    }

4.设置较小高度标签（一般小于10px），在IE6，IE7中高度超出自己设置高度。hack：给超出高度的标签设置overflow:hidden;或者设置行高line-height 小于你设置的高度。

5.IE下，可以使用获取常规属性的方法来获取自定义属性,也可以使用getAttribute()获取自定义属性；Firefox下，只能使用getAttribute()获取自定义属性。解决方法:统一通过getAttribute()获取自定义属性。

6.Chrome 中文界面下默认会将小于 12px 的文本强制按照 12px 显示,可通过加入 CSS 属性 -webkit-text-size-adjust: none; 解决。

7.超链接访问过后hover样式就不出现了，被点击访问过的超链接样式不再具有hover和active了。解决方法是改变CSS属性的排列顺序:L-V-H-A ( love hate ): a:link {} a:visited {} a:hover {} a:active {}

**二十四、 浏览器是怎样解析CSS选择器的**

​        CSS选择器的解析是从右向左解析的。若从左向右的匹配，发现不符合规则，需要进行回溯，会损失很多性能。若从右向左匹配，先找到所有的最右节点，对于每一个节点，向上寻找其父节点直到找到根元素或满足条件的匹配规则，则结束这个分支的遍历。两种匹配规则的性能差别很大，是因为从右向左的匹配在第一步就筛选掉了大量的不符合条件的最右节点（叶子节点），而从左向右的匹配规则的性能都浪费在了失败的查找上面。

​        而在 CSS 解析完毕后，需要将解析的结果与 DOM Tree 的内容一起进行分析建立一棵 Render Tree，最终用来进行绘图。在建立 Render Tree 时（WebKit 中的「Attachment」过程），浏览器就要为每个 DOM Tree 中的元素根据 CSS 的解析结果（Style Rules）来确定生成怎样的 Render Tree。

移动端页面头部必须声明有meta声明的viewport:

**26 、移动端页面头部必须有meta声明的viewport**

> <meta name="’viewport’" content="”width=device-width," initial-scale="1." maximum-scale="1,user-scalable=no”"/>

**二十七、 position:fixed;在android下无效怎么处理**



> <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"/>

**二十八、如果需要手动写动画，你认为最小时间间隔是多久，为什么**

​        多数显示器默认频率是60Hz，即1秒刷新60次，所以理论上最小间隔为1/60＊1000ms ＝ 16.7ms。

**二十九、 li与li之间有看不见的空白间隔是什么原因引起的？有什么解决办法？**

​        行框的排列会受到中间空白（回车空格）等的影响，因为空格也属于字符,这些空白也会被应用样式，占据空间，所以会有间隔，把字符大小设为0，就没有空格了。
解决方法：
​        可以将    ：   代码全部写在一排
​        浮动li中float：left
​        在ul中用font-size：0（谷歌不支持）；可以使用letter-space：-3px

**三十、 png、jpg、gif 这些图片格式解释一下，分别什么时候用。有没有了解过webp？**

​        png是便携式网络图片（Portable Network Graphics）是一种无损数据压缩位图文件格式.优点是：压缩比高，色彩好。 大多数地方都可以用。

​        jpg是一种针对相片使用的一种失真压缩方法，是一种破坏性的压缩，在色调及颜色平滑变化做的不错。在www上，被用来储存和传输照片的格式。

​        gif是一种位图文件格式，以8位色重现真色彩的图像。可以实现动画效果.

​        webp格式是谷歌在2010年推出的图片格式，压缩率只有jpg的2/3，大小比png小了45%。缺点是压缩的时间更久了，兼容性不好，目前谷歌和opera支持。

**三十一、CSS属性overflow属性定义溢出元素内容区的内容会如何处理?**

​        参数是scroll时候，必会出现滚动条。

​        参数是auto时候，子元素内容大于父元素时出现滚动条。

​        参数是visible时候，溢出的内容出现在父元素之外。

​        参数是hidden时候，溢出隐藏。

## **css的复合选择器与特性**

在本篇学习资料中，将深入了解css的相关概念，上一篇介绍的3种基本选择器的基础上，学习3种由基本选择器复合构成的选择器，然后再介绍css的两个重要的特性。

**1、****复合选择器**

复合选择器就是两个或多个基本选择器，通过不同的方式连接成的选择器。

复合选择器的三种类型：交集选择器、并集选择器、后代选择器。

**（1）交集选择器**

“交集”复合选择器由两个选择器直接连接构成；其中第一个必须是标记选择器，第二个必须是类别选择器或者ID选择器；这两个选择器之间不能有空格。

**（2）并集选择器**

并集选择器简称“集体声明”；并集选择器是多个选择器通过逗号连接而成的。

**（3）后代选择器**

 css选择器中，还可以通过嵌套的方式对特殊位置的html 标记进行声明，例如当<p>与</p>之间包含<span></span>标记时，就可以使用后代选择器进行相应的控制。后代选择器的写法就是把外层的标记写在前面，内层的标记写在后面，之间用空格分隔。当标记发生嵌套时，内层的标记就成为外层标记的后代。

## js基础语法，document，date对象常用方法的使用方法

### js基础语法

1. 1、分类

2. ```
   ECMAScript  js基本语法与标准
   DOM         Document Object Model文档对象模型
   BOM         Browser Object Model浏览器对象模型
   
       tips：DOM和BOM都是一套API（Application programing interface）
   ```

3. ## 2、注释方式

4. ```
   style   /*  */
   body    <!-- --!>
   script  //
           /* */
           /**
           *   js说明文档注释
           */
   ```

5. ## 3、简单指令

6. ```
   alert("");          提示框；
   confirm("");        确认框，点击后会响应返回true或false；             
   prompt();           弹出一个输入框；
   document.write("");
   console.log("");    在控制台打印相应的信息；
   console.dir("");    在控制台打印出该对象的所有信息；
   ```

7. ## 4、变量命名

8. ```
   数字（0-9）、字母（a-z，A-Z）、下划线（_）；
       tips:应避免保留字和关键字；
   ```

9. ## 5、NaN和isNaN

10. ```
    isNaN(number),如果number不是数字，则为true；
    Number(number),在转换为数字类型时，若number不是数字，则返回NaN；
    ```

11. ## 6、转义字符

12. ```
    \       
    \r  回车
    \n  空格
    \t  缩进
    \\  反斜杠
    ```

13. ## 7、逻辑短路、逻辑中断

14. ```
    true || 6;      逻辑或短路，左边为ture返回右值；
    6   &&  true;   逻辑与短路，左边false返回右值；
    ```

15. ## 8、优先级

16. ```repl
        * / %
        +   -
        &&
        ||
        ?
    tips：自上而下优先级越来越高
    ```

17. ## 9、类型转换（type）

18. ```
    parseInt("12a3");   转为数字，尝试强转；
    parseFloat("123.123");
    
    data.toString();
    String(data);
    
        tips:变量声明未赋值，其值为undefined；
            对象为空，其值为null；
    ```

19. ------

20. # 10、三元表达式

21. ```
    eg  :   a>b?a=1:a=2;
    
    格式:
        判断条件？true的时候执行的操作：false的时候执行的操作；
    ```

22. ## 11、数组Array

23. ```
    （1）、定义法
        构造函数：
                var arr = new Array("123","abc","xxx");
        字面量：
                var arr = ["123","646","abc"]; 
        数组长度：
                var arr = new Array(6);(数组长度为6)；
    （2）、赋值
        arr[0]=1;
    ```

24. ## 12、形参和实参

25. ```
    定义函数时，function funcA(a,b,c){}，其中的a、b、c即为形参；
    调用函数时，funcA(1,2,3);其中的1、2、3即为实参；
    
    tips：function里面有一个arguments对象，里面存有所有传进函数的实参；
    ```

26. ------

27. # 13、函数function

28. ```
    （1）、函数命名
        1、  可以使用字符、数字、下划线、$；
        2、  不能以数字开头；
        3、  不能使用关键字和保留字；
        4、  区分大小写；
    
        5、  建议要有意义 --  动词+名字结构；
        6、  驼峰命名法；
        7、  函数名不能重名，后面写的重名函数会把前面写的函数给覆盖掉；
    
    （2）、函数的返回值
    返回值：
        当函数执行完毕之后，所得到的结果就是一个函数返回值
        任意函数都有返回值
    
    1、  在函数内部没有显示的写有return的时候，函数的返回值是undefined；
    2、  当函数内部有return，但是return后面没有跟着任何内容或者数据的时候，
    函数的返回值是undefined，并且return后面的代码不会执行；
    3、  当return后面跟着内容或者数据的时候，函数的返回值就是这个跟着的内容或者数据；
    
    
    （3）、函数的四种形式：
        1、没有参数，没有return；
                通常在于封装一段过程；
        2、没有参数，有return；
                通常用于内部封装引用其他函数（闭包，回调）；
        3、有参数，没有return；
                通常用于执行操作的封装；
        4、有参数，有return；
                常见形式；
    
    （4）、匿名函数
        匿名函数的name属性值为anonymous；
        函数仅用一次的情况，即用即废；
    
        eg:
            setTimeout(function(){
                console.log(this.name);
            },1000);
        tips:在1秒后在控制台打印出本函数的名称；
    
    （5）、回调函数
        在一个函数当中，另一个函数作为参数传入该函数中，另一个的这个函数即为回调函数；
        eg:
            function atack(callback){
                return callback;
            }
        tips:在调用该函数时，指定callback是哪个函数；
            atack（func）；
    
    （6）、短路运算
        作用：防止传入函数的数据不足，造成无法运行；
        eg：
            function getResult(a,b,fn) {
                fn && fn();
            }（通常使用逻辑与的短路来决定是否执行回调函数；）
    
            function getResult_2(a,b){
                a || 0;
            }（通常用逻辑或的短路来防止实参不足的情况，强行赋值；）
    
    （7）、自执行函数
        （function func2(){
    
        }）()
    
        tips:在函数定义的结束最后写入一个（），该函数定义完成后直接被调用执行；
    
    （8）、递归
        在函数执行的最后再一次的调用自身；
    
        tips:递归是一种非常耗资源的做法，通常为了简化运算，还会结合缓存进行；
        并且注意，递归必须要有结束判断条件（if），否则该函数被调用后就是死循环；
    ```

29. ## 14、数据类型

30. ```
    （1）、简单数据类型
        string、number、boolean
    
    （2）、复杂数据类型
        String、Number、Boolean、Array、Math、Date、Obeject、function、RegExp(正则表达式)
    
    （3）、空数据类型
        * Null  ---→Null的数据类型会返回一个Object
        * undifined
    
        tips：用typeof可以进行判断数据类型；
    
        tips：定义的简单数据类型变量，其数据保存在变量中；
            而复杂数据类型，其变量保存的是数据所在的内存地址；
    ```

31. ## 15、内置对象

32. ```
    Array、Date、Math、String；
    ```

33. ## 16、（Math）数学对象

34. ```
    向上取整        Math.ceil(number);
    向下取整        Math.floor(number);
    
    四舍五入        Math.round(number);
    
    求多个数字之间的最大值     Math.max();
    求多个数字之间的最小值     Math.min();
    
    求x的y次幂      Math.pow(x,y);
    
    求正弦值            Math.sin(x);
        example:
            求一个角度的正弦值，要求x必须是一个额弧度值
            角度和弧度的转换公式：
                弧度 = 角度 * 2 * Math.PI / 360;
    
            Math.sin(30*2*Math.PI/360)
    
    Math.abs(x);    得到一个数字的绝对值
    ```

35. ## 17、（Array）数组对象

36. ```
    （1）、arr1.concat(arr2);
            数组拼接，结果为将arr2拼接到arr1的最后；
    
    （2）、arr.join()；
            数组字符串输出，括号内可以指定元素连接的符号；
            eg:
                arr=["a","b","c","d"];
                console.log(arr.join("|"));     (结果为"a|b|c|d")
    
    （3）、arr.pop();
            切除数组的最后一个元素，返回值为该元素；
    
    （4）、arr.slice(start,end)
            获取，获取数组的指定片段，start必须有，如果参数为负数则从末尾开始选取；
            返回值为该片段组成的，一个新的数组；
    
    （5）、arr.push
            添加，用于向数组的末尾添加新的元素，参数可以是多个；
            返回值为数组的新长度；
    
    （6）、arr.splice
            1、用于向数组中指定的索引添加元素；
                arr.splice(2, 0, "William","asdfasdf");
                    在第2个元素开始，删除的元素个数（可以为0，为0到结尾），
                    加入元素为"William"、"asdfasdf"；
    
            2、用于替换数组中的元素；
                arr.splice(2,1,"William")；          
    
            3、用于删除数组中的元素；
                 arr.splice(2,2);
    
    （7）、arr.indexOf(element);
            查找，在数组中查找element，返回值为索引，如果没有该元素返回-1；
    
    （8）、arr.sort(function);
            排序，function为一个函数；
                eg:
                    function sortNumber(a,b){
                        return a-b;
                    }
                    arr.sort(sortNumber);(从小到大排序)
    
        tips：如果a-b改成b-a，那么执行的操作为从大到小；
        tips:字符串对象（String）的方法与Array的方法类似；
    ```

37. ## 18、（Date）日期对象

38. ```
    date.getTime()
    date.getMilliseconds()
    date.getSeconds()
    date.getMinutes()
    date.getHours()
    date.getDay()
    date.getDate()
    date.getMonth()
    date.getFullYear()
    
    tips:很多，查文档
    ```

39. ## 19、（String）对象

40. ```
    charAt(index)
    str[index]          获取字符串指定位置的字符
    
    concat()        拼接字符串
    ---------------------------
    slice(start,end)/
    substring(start,end)    截取从start开始，end结束的字符，
                    返回一个新的字符串，若start为负数，那么从最后一个字符开始；
    
    substr(start,length)    截取从start开始，length长度的字符，得到一个新的的字符串
    ---------------------------
    
    indexOf(char)       获取指定字符第一次在字符串中的位置
    lastIndexOf(char)   获取指定字符最后一次出现在字符串中的位置
    
    trim()      去除字符串前后的空白
    ---------------------------
    toUpperCase()
    toLocaleUpperCase()     转换为大写
    
    toLowerCase()
    toLocaleLowerCawse()    转换为小写
    ---------------------------
    
    replace()       替换字符
    split()         分割字符串为数组
    ```

41. ## 20、自定义对象

42. ```
    对象：无序属性的集合；
        特征：属性（key）；
        行为：方法（value）；
    
    js是基于对象的弱类型语言；
    
    继承：基于类，子类可以从父类得到的特征；    
    
    工厂模式：定义一个function构造函数，作为对象，要创建对象直接调用该构造函数，加new关键字；
    
    构造函数：定义对象的函数，里面存有该对象拥有的基本属性和方法；
        命名首字母大写，this会自动指代当前对象；
    
    访问对象属性：
        obj[key];
        obj.key;
    
    遍历对象：
        for(key in obj){
            key         为属性名；
            obj[key]    为属性值（value）；
        }
    ```

43. ## 21、JSON

44. ```
    {
       "name" : "李狗蛋",
       "age" : 18,
       "color" : "yellow"
    }
    
    1、  所有的属性名，必须使用双引号包起来；
    2、  字面量侧重的描述对象，JSON侧重于数据传输；
    3、  JSON不支持undefined；
    4、  JSON不是对象，从服务器发来的json一般是字符串，
    通过JSON.parse(jsonDate.json)可以将其转换成js对象；
    ```

45. ## 22、JS解析

46. ```
    （1）、作用域
    全局作用域：整个代码所有地方都可以调用；
    局部作用域：在函数内部声明的变量，只可以在函数内部使用；
    
    （2）、变量提升和函数提升
    预解析：在解析的时候，var和function都会被提升到代码的最顶端；
        但是赋值操作不会被提升，定义和函数才会被提升；
        if里面的变量定义也会被提升，但是赋值操作不会；
    ```

47. ## 23、其他细节（tips）

48. ```
    (1)、元素由对象组成的数组进行排序
        eg：
            var data = [
                {title: "老司机", count: 20},
                {title: "诗人", count: 5},
                {title: "歌手", count: 10},
                {title: "隔壁老王", count: 30},
                {title: "水手", count: 7},
                {title: "葫芦娃", count: 6},
            ];
                //该数组的元素都为对象。我们需求为根据count的值给数组重新排序。
                //解决方案：使用sort方法，对传入的函数做手脚。
    
            function sortArr(a,b){
                return a.count > b.count;   
            }
            data.sort(sortArr);
    
                //原本的a和b的比较方法变成a.count和b.count；
                //原本的比较方法可以参见17，数组对象
                //至此，data将以count值从小到大排列。
    
        tips:Array对象的sort方法传入的为比较函数，比较函数里return排序比较的方法；
            原始的sort方法传入的函数内部return的值为a>b，
            通过修改sort传入的函数，可以实现对元素为对象的数组的排序！
    ```

### 一、document对象


文档对象（document）代表浏览器窗口中的文档，该对象是window对象的子对象，由于window对象是DOM对象模型中的默认对象，因此window对象中的方法和子对象不需要使用window来引用。
通过document对象可以访问HTML文档中包含的任何HTML标记并可以动态的改变HTML标记中的内容。
例如表单、图像、表格和超链接等。


document 对象的属性
document对象主要有如下属性：

属性	说明
document.title	设置文档标题等价于HTML的<title>标签
document.bgColor	设置页面背景色
document.linkColor	未点击过的链接颜色
document.alinkColor	激活链接(焦点在此链接上)的颜色
document.fgColor	设置前景色(文本颜色)
document.vlinkColor	已点击过的链接颜色
document.URL	设置URL属性从而在同一窗口打开另一网页
document.fileCreatedDate	文件建立日期，只读属性
document.fileModifiedDate	文件修改日期，只读属性
document.fileSize	文件大小，只读属性
document.cookie	设置和读出cookie
document.charset	设置字符集 简体中文:gb2312


document.referrer                                                  返回载入当前文档的文档的url
document 对象的常用方法

常用方法	说明
document.write()	动态向页面写入内容
document.createElement(Tag)	创建一个html标签对象
document.getElementById(ID)	获得指定ID值的对象     （   对象ID唯一  ）
document.getElementsByTagName(tagname)	获得指定标签名的对象的集合 （相同的元素）
document.getElementsByName(Name)  	
获得指定Name值的对象的集合   （相同name属性）                      

avaScript支持的对象主要包括以下三大类：内置对象，如Date、Math、String、Array、等；浏览器对象，如：Window、Location、Histor、Screen、Document等；用户自定义的对象。今天我们来讲解js的日期时间Date对象。

### **1. 新建Date对象的3种方式**

要用Date对象，必须先创建对象，将它实例化，才可以用它的属性和方法。

（1） var mytime1=new Date( )； //打印的是系统当前时间

（2） var mytime2=new Date("2019/3/19" ) ；

（3） var mytim3e=new Date(4600); //注意参数的4600是毫秒，且没有加双引号

然后我们分别将这3个对象用document.write()打印出来，结果如下：

![img](https://pics6.baidu.com/feed/7dd98d1001e93901d7f60b4b3110dde334d196d4.jpeg?token=336ce56e97d8be55df8c9aba629749ee)mytime1

![img](https://pics1.baidu.com/feed/d009b3de9c82d158f3a1fa07caf690dcbd3e4261.jpeg?token=405c33e64e71ab53364de9eb69a04b3a)mytime2

![img](https://pics3.baidu.com/feed/4b90f603738da977f82ec077faad711d8718e36e.jpeg?token=f072f9fc3b777dcc9d9d3d4c46606c9e)

### **2. Date对象常用方法**

调用Date对象的方式：**对象名.方法名()。**

**getFullYear()：获取对象的年份方法**，获取到的年份一般是4位数，如2019。

**getMonth()：获取对象的月份**，特别注意返回的月份取值范围是**0-11**，0表示的是我们中文的1月，所以当我们打印出月份是通常这个函数都要加1，如下边的程序。

**getDate()：获取对象的日期数**。范围是1-31，这个方法容易与getDay()混淆，getday()是获取对象的星期数，他的范围是0-6，0表示的是我们中文的星期天，1表示的星期一，以此类推。

**getHours()：获取小时数**，范围0-23。

**getMinutes()：获取分钟数**，范围0-59。

**getSeconds():获取秒数**，范围0-59。

![img](https://pics5.baidu.com/feed/42166d224f4a20a446c06026d8ae1126700ed071.jpeg?token=08d91473fcdbbd051e749fe8dd6c6310&s=EFE2B357CDA44C0118E5045E02009072)

输出结果是：

![img](https://pics3.baidu.com/feed/2fdda3cc7cd98d10ec1585e469c3310a79ec9027.jpeg?token=6834b02c1e3ad399db93806fb380bb08&s=C4107593CDA05D01185DA4DA010090B2)

现在，大家可以看到这个输出的时间是静态的，并不能随系统时间动态变化，那怎样才可以让时间动态变化起来呢？欢迎关注我们下一次的内容：让时间动起来。

---------------------
## form与input标签常用属性及使用的方法

一、form相关属性 
1.可以用name属性给表单进行命名 
2.编码方式enctype用于设置表单信息提交时的编码方式 
3.target制定目标窗口的打开方式 
二、表单的input对象 
1.text，password类型input常用的属性size（输入框大小），maxlength（最多输入长度） 
2.单选框将type设置为radio,name属性相同的一组为一组单选框 
3.复选框的type为checkbox 其余大致同上，可以多选 
4.按钮button，value为按钮的取值，onclick实现一些特殊的功能（如关闭窗口），value为要显示的按钮提示 
5.提交按钮submit基本属性同上，点击提交表单 
6.图像域image，可以使用一幅图像作为按钮， 
7.隐藏域hidden，在前端页面中不会显示 

8.文本域 textarea cols,rows指定列数和行数 

## 盒子模型：

# CSS 盒子模型

------

### CSS 盒子模型(Box Model)

所有HTML元素可以看作盒子，在CSS中，"box model"这一术语是用来设计和布局时使用。

CSS盒模型本质上是一个盒子，封装周围的HTML元素，它包括：边距，边框，填充，和实际内容。

盒模型允许我们在其它元素和周围元素边框之间的空间放置元素。

下面的图片说明了盒子模型(Box Model)：



不同部分的说明：

- **Margin(外边距)** - 清除边框外的区域，外边距是透明的。
- **Border(边框)** - 围绕在内边距和内容外的边框。
- **Padding(内边距)** - 清除内容周围的区域，内边距是透明的。
- **Content(内容)** - 盒子的内容，显示文本和图像。

为了正确设置元素在所有浏览器中的宽度和高度，你需要知道的盒模型是如何工作的。

------

### 元素的宽度和高度

![Remark](https://www.runoob.com/images/lamp.gif)**重要:** 当您指定一个CSS元素的宽度和高度属性时，你只是设置内容区域的宽度和高度。要知道，完全大小的元素，你还必须添加填充，边框和边距。.

下面的例子中的元素的总宽度为300px：

### 实例

```css
div {     width: 300px;    
    border: 25px solid green;  
    padding: 25px;   
    margin: 25px; 
}
```



尝试一下 »

让我们自己算算：
300px (宽)
\+ 50px (左 + 右填充)
\+ 50px (左 + 右边框)
\+ 50px (左 + 右边距)
= 450px

试想一下，你只有250像素的空间。让我们设置总宽度为250像素的元素:

### 实例

```css
div {     
    width: 220px;  
    padding: 10px;
    border: 5px solid gray; 
    margin: 0; 
}
```



尝试一下 »

最终元素的总宽度计算公式是这样的：

总元素的宽度=宽度+左填充+右填充+左边框+右边框+左边距+右边距

元素的总高度最终计算公式是这样的：

总元素的高度=高度+顶部填充+底部填充+上边框+下边框+上边距+下边距

------

### 浏览器的兼容性问题

一旦为页面设置了恰当的 DTD，大多数浏览器都会按照上面的图示来呈现内容。然而 IE 5 和 6 的呈现却是不正确的。根据 W3C 的规范，元素内容占据的空间是由 width 属性设置的，而内容周围的 padding 和 border 值是另外计算的。不幸的是，IE5.X 和 6 在怪异模式中使用自己的非标准模型。这些浏览器的 width 属性不是内容的宽度，而是内容、内边距和边框的宽度的总和。

虽然有方法解决这个问题。但是目前最好的解决方案是回避这个问题。也就是，不要给元素添加具有指定宽度的内边距，而是尝试将内边距或外边距添加到元素的父元素和子元素。

IE8 及更早IE版本不支持设置填充的宽度和边框的宽度属性。

解决IE8及更早版本不兼容问题可以在HTML页面声明 <!DOCTYPE html>即可。

## 外部资源引用的路径问题：

[jsp、css中引入外部资源相对路径的问题]

在jsp页面中添加base，可用相对路径：

```
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
```

 

然后在<head>标签内添加base

```
<base href="<%=basePath%>"></base>
```

 

　在本页面中“直接”引入外部文件时，可直接

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
<script src="js/common/jquery-1.11.1.min.js" language="javascript"
    type="text/javascript"></script>
<script src="js/common/frame.js" language="javascript"
    type="text/javascript"></script>
<link href="css/common/frame.css"
    rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

同理，本页面中的css类

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
.top {
    position: absolute;
    left: 0;
    top: 95px;
    right: 0;
    height: 120px;
    background: url(images/common/title.jpg) repeat-x
}
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

====================例外情况：引入的外部css、js文件又引入image等时=================================

这时候如果也用相对路径的话，因为已经不在了jsp页面中，此时相对路径是相对于本css文件所在的目录：

 

如：index.jsp页面中引入的css文件

```
<link href="css/common/frame.css" rel="stylesheet" type="text/css" />
```

这时在jsp页面中，引入的js/common/frame.js是从网站跟目录开始寻找，没问题。

但在frame.js中有如下css

```
.show_menu{
    background-image: url(images/left_bg.gif);
    background-repeat: repeat-y;
    background-position:285px 51px;
}
```

这种情况下直接url(images/left_bg.gif);是默认从本css文件所在目录（/css/common/frame.css）+url(images/left_bg.gif)，所以此处需要的image需要单独配置，

改成

```html
.show_menu{
    background-image: url(../../images/left_bg.gif);
    background-repeat: repeat-y;
    background-position:285px 51px;
}
```

## 页面布局相关知识，display,float,position,清除浮动的方法

### html+css布局必备基础知识汇总

## 1. **布局必备基础知识**

### 1.1 **标签类型及特性**

#### 1.1.1 **行内元素（内联元素）**

1. 常见的行内标签有：

> a、span、strong、b、em、i等。

1. 内联元素特点：

> - 和其他元素都在一行上；
> - 高及外边高，行距和内边距部分可改变；
> - 宽度只与内容有关；
> - 行内元素只能容纳文本或者其他行内元素。

#### 1.1.2 **行内块元素**

1. 常见的行内块元素有：

> img、 input 、 button、 select、 textarea等。

1. 行内块元素特点：

> - 内部表现为块级元素,可设置宽高，支持盒模型。
> - 外部表现为行内元素 不独占一行，从左到右排列。

#### 1.1.3 **块级元素**

1. 常见的块级元素有：

> div 、 section 、 ul 、 ol 、 dl 、 table 、form等。

1. 块级元素特点：

> - 总是在新行上开始，占据一整行；
> - 高度，行高以及外边距和内边距都可控制；
> - 宽始终是与浏览器宽度一样，与内容无关；
> - 它可以容纳内联元素和其他块元素。

#### 1.1.4 **标签类型转换**

行内大多为描述性标记，块级大多为结构性标记。
通过设置`display:inline/inline-block/block`可以修改标签的类型。

### 1.2 **盒模型**

#### 1.2.1 **标准盒模型（w3c）**



![img](https://upload-images.jianshu.io/upload_images/3732356-e00b75426e4004c4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/449/format/webp)

image.png

#### 1.2.2 **怪异盒模型（ie）**



![img](https://upload-images.jianshu.io/upload_images/3732356-60907e9b9abc15d9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/443/format/webp)

image.png

#### 1.2.3 **盒模型的转换**

为了保证网页在各个浏览器中显示一致，通常设置doctype声明，规定浏览器都按照w3c标准盒模型去渲染。假如不加 doctype 声明，那么各个浏览器会根据自己的行为去理解网页。

通过设置`box-sizing:content-box/border-box`可以转换盒模型。

### 1.3 **定位（position）**

#### 1.3.1 **普通流定位(static)**

Position的默认值。没有定位，元素出现在正常的文档流中
（忽略 top, bottom, left, right 或者 z-index 声明）。

#### 1.3.2 **相对定位(relative)**

生成相对定位的元素，相对于其正常位置进行定位。
元素仍然保持其未定位前的形状，它**原本所占的空间仍保留**。

> **注意**:
> 在使用相对定位时，无论是否进行移动，元素仍然占据原来的空间。 因此，移动元素会导致它覆盖其它标准文档流的框。

#### 1.3.3 **绝对定位(absolute)**

绝对定位的元素的位置相对于最近的已定位祖先元素，
如果元素没有已定位的祖先元素，那么它的位置相对于最初的包含块。
绝对定位的元素框从文档流完全删除，**因此不占据空间**。

#### 1.3.4 **固定定位(fixed)**

固定定位的元素，相对于浏览器窗口进行定位。 将元素的内容固定在页面中的某个位置，元素从普通文档流中完全移除，不占用页面空间，当用户向下滑动页面元素框时并不随着移动
（固定在窗口的某处）。

> 相对定位是“相对于”元素在文档中的初始位置定位，而绝对定位是“相对于”最近的已定位祖先元素，如果不存在已定位的祖先元素，那么“相对于”最初的包含块进行定位。

### 1.4 **浮动（float）**

#### 1.4.1 **浮动的目的**

最初时，浮动只能用于图像（某些浏览器还支持表的浮动），目的就是为了允许其他内容（如文本）“围绕”该图像,而后来的CSS允许浮动任何元素。

#### 1.4.2 **浮动的特点**

浮动定位是将元素排除在普通标准文档流之外，元素将不占用空间，无法撑开父元素；

在没有定义具体宽度的情况下，使自身的宽度从100%变为自适应（浮动元素display:block，但是展现形式是display:inline-block）

浮动框可以向左或向右移动，直到它外边碰到包含框或者另一个浮动框； 浮动元素的外边缘不会超出其父元素的内边缘；

浮动的元素不会互相重叠，浮动元素也不会上下浮动（不类似z-index）。

#### 1.4.3 **清除浮动**

由于浮动会使当前标签产生向上浮的效果，同时会影响到前后标签、父级标签的位置及 width height 属性。而且同样的代码，在各种浏览器中显示效果也有可能不相同，所以清除浮动很有必要。

[8种CSS清除浮动的方法优缺点分析](http://www.5icool.org/a/201211/a1661.html)

### 1.5 **Flex/Grid布局**

Flex是Flexible Box的缩写，意为”弹性布局”，用来为盒状模型提供最大的灵活性，亦可称其为“弹性盒模型”。

[Flex布局 语法篇](http://www.ruanyifeng.com/blog/2015/07/flex-grammar.html)
[Flex布局 实例篇](http://www.ruanyifeng.com/blog/2015/07/flex-examples.html)

[Grid布局 语法篇](http://www.css88.com/archives/8506)
[Grid布局 实例篇](http://www.css88.com/archives/8510)

Flex布局与Grid布局的对比

- CSS grids 适用于布局大画面。它们使页面的布局变得非常容易，甚至可以处理一些不规则和非对称的设计。2D 布局适合使用 CSS grids（行与列）。

- Flexbox 非常适合对齐元素内的内容。你可以使用 Flex 来定位设计上一些较小的细节。Flexbox 适用于单一维度的布局（行或列）。

  [Flex布局与Grid布局的对比](https://www.zhihu.com/question/28691822)

## 2. **布局类型及常见布局**

### 2.1 **布局类型**

#### 2.1.1 **静态布局**

传统Web设计，对于PC设计一个Layout，在屏幕宽高有调整时，使用横向和竖向的滚动条来查阅被遮掩部分。

#### 2.1.2 **自适应布局**

特点是分别为不同的屏幕设置布局格式，当屏幕大小改变时，会出现不同的布局，意思就是在这个屏幕下这个元素块在这个地方，但是在那个屏幕下，这个元素块又会出现在那个地方。只是布局改变，元素并不变(页面元素不随窗口大小的调整发生变化)。可以看成是不同屏幕下由多个静态布局组成的。(技术点：媒体查询)

#### 2.1.3 **流式布局**

特点是随着屏幕的改变，页面的布局没有发生大的变化，可以进行适配调整，这个正好与自适应布局相补。(不固定宽高，使用百分比)

#### 2.1.4 **响应式布局**

分别为不同的屏幕分辨率定义布局，同时，在每个布局中，应用流式布局的理念，即页面元素宽度随着窗口调整而自动适配。 可以把响应式布局看作是流式布局和自适应布局设计理念的融合。

### 2.2 **常见布局方式**

[常见布局方式](https://segmentfault.com/a/1190000003931851#articleHeader0)

#### 2.2.1 **单列布局**

#### 2.2.2 **多列布局**

#### 2.2.3 **全屏布局**

[全屏布局](http://www.cnblogs.com/xiaohuochai/p/5458068.html)

#### 2.2.4 **响应式布局**

### 2.3 **布局常用的技术**

定位(绝对，相对)、浮动、flex布局、margin、table、@media媒体查询

## 3. **布局常见问题**

### 3.1 **水平垂直居中问题**

[水平垂直居中问题](https://www.cnblogs.com/haoyijing/p/5815394.html)

### 3.2 **清除浮动的问题**

[8种CSS清除浮动的方法优缺点分析](http://www.5icool.org/a/201211/a1661.html)

```
     常用清除浮动方法

        .clearfix:before,
        .clearfix:after {
            content: "";
            display: table;
        }
        .clearfix:after {
            clear: both;
        }
        .clearfix {
            *zoom: 1;
        }
```

### 3.3 **文本溢出隐藏**

```
1. 单行文本溢出

       .overflow_ellipse{
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

2. 多行文本溢出

       .overflow_ellipse{
          overflow : hidden;
          text-overflow: ellipsis;
          display: -webkit-box;
          -webkit-line-clamp: 要显示的行数;
          -webkit-box-orient: vertical ;
        }
```

### 3.4 **其它**

1. 什么时候用padding和margin
   Padding是做内容距边框之间留白的，
   Margin是做容器与容器之间留白的。

2. Css的继承属性有哪些？如何使子元素不具有父元素的属性？
   给子元素重写父元素的属性，并设置成自己的属性值

   ### css(display,float,position)深入理解

   **display 用来设置元素的显示方式**

   display : block | none | inline | inline-block

   inline：指定对象为内联元素

   block：指定对象为块元素

   inline-block：指定对象为内联块元素

   none：隐藏对象

   **float 控制元素是否浮动显示**

   float : none | left | right

   none：设置对象不浮动

   left：设置对象浮在左边

   right：设置对象浮在右边

   **浮动的目的：**

   就是要打破文档流的默认显示规则。如果要让元素按照我们的布局要求进行显示。这时就要利用float属性

   1.任何申明为 float 的元素自动被设置为一个“块级元素”

   2.在标准浏览器中浮动元素脱离了文档流 ，所以浮动元素后的元素会占据浮动元素本来应该所处的位置

   3.如果水平方向上没有足够的空间容纳浮动元素，则转向下一行

   4.文字内容会围绕在浮动元素周围

   5.浮动元素只能浮动至左侧或者右侧

   **clear 清除浮动**

   clear : none | left | right | both

   none：默认值。允许两边都可以有浮动对象

   left：不允许左边有浮动对象

   right：不允许右边有浮动对象

   both：不允许有浮动对象

   **position 对象的定位方式**

   position : static | absolute | fixed | relative

   static：默认值。无定位，对象遵循常规流。此时4个定位偏移属性不会被应用

   relative：相对定位，对象遵循常规流，并且参照自身在常规流中的位置通过top，right，bottom，left这4个定位偏移属性进行偏移时不会影响常规流中的任何元素

   absolute：绝对定位，对象脱离常规流，此时偏移属性参照的是离自身最近的定位祖先元素，如果没有定位的祖先元素，则一直回溯到body元素。盒子的偏移位置不影响常规流中的任何元素，其margin不与其他任何margin折叠

   fixed：固定定位，与absolute一致，但偏移定位是以窗口为参考。当出现滚动条时，对象不会随着滚动

   **absolute 说明：**

   1.脱离文档流

   2.通过 top,bottom,left,right 定位

   3.如果父元素 position 为 static 时，将以body坐标原点进行定位

   4.如果父元素 position 为 relative 时，将以父元素进行定位

   例：div { position: absolute; left:100px; top:100px;}

   **relative 说明：**

   1.相对定位（相对自己原来的位置而言）

   2.不脱离文档流

   3.参考自身静态位置通过 top,bottom,left,right 定位

   例：div { position: relative; left:100px; top:100px;}

   fixed 说明：

   固定定位实际上只是绝对定位的特殊形式，固定定位的元素是相对于浏览器窗口而固定，而不是相对于其包含元素，即使页面滚动了，它们仍然会处在浏览器窗口中跟原来完全一 样的地方

   例：div { position: fixed; right:0; bottom:0;}

   **z-index 对象的层叠顺序**

   z-index : auto | number

   当元素发生重叠时，可以通过 z-index 属性，设置其层叠的先后顺序

   较大 number 值的对象会覆盖在较小 number 值的对象之上

   ![img](https://files.jb51.net/file_images/article/201608/201608170910402.png)
---
title: Java中使用JNA实现全局监听Linux键盘事件
date: 2019-05-03 19:08:00
categories:
	- [java]
tag:
	- Java
	- JNA
author: 洪凯庆
---

Java中使用JNA实现全局监听Linux键盘事件

<!--more-->

​		用JNA实现的键盘监听，在Windows上完美运行，能在后台拿到键盘输入的内容，可在Linux下还是没办法，于是在网上各种找，又找到两种办法，还是一样，在Windows上运行都没问题，最后都失败在Linux，最后，我看失败的错误描述，貌似问题都出在了图形界面X11上面还有什么键盘布局上，我的deppin系统啊

一、使用jnativehook监听键盘输入

​		jnativehook也是个开源项目，项目地址：[kwhat/jnativehook](https://github.com/kwhat/jnativehook)，这个项目也是全平台支持的，Windows，Linux、Mac全都支持，但是Linux下貌似是需要装X11，然后键盘布局也得是相关，不然报错了。我查了一下我现在使用的Linux（deppin）系统刚好可以用，貌似装了X11。

​		这个项目使用也挺简单的，从他的[仓库](https://oss.sonatype.org/content/repositories/snapshots/com/1stleg/jnativehook/2.1.SNAPSHOT/)下载JAR包，下载下来，用命令运行java -jar jnativehook.jar，就能出现一个用Swing绘制的图形小程序demo，简单演示监听到的鼠标键盘等输入，具体怎么调用的，将jar包放入eclispe，查看包内的[src/java/org/jnativehook/example/NativeHookDemo.java](https://github.com/kwhat/jnativehook/blob/2.1/src/java/org/jnativehook/example/NativeHookDemo.java)文件就可以看到了。项目要引用，也只需要这一个jar包导入就可以，实现方法也很简单，跟Swing程序中鼠标键盘监听一模一样。下面贴上我简单实现的鼠标监听的代码，其他鼠标监听之类的可以自己研究下官方Demo源码，很明了。

```java
package Soundeffects.Hkq;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.dispatcher.DefaultDispatchService;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import java.util.logging.Logger;

/**
 * Demo LinuxKeyBoard
 *linux系统键盘事件监听，使用了lib目录下的jnativehook.jar
 * @author hkq
 * @date 2019/05/02
 */
public class LinuxKeyBoard  implements NativeKeyListener {

    private static final Logger logger = Logger.getLogger( GlobalScreen.class.getPackage().getName());

    /**
     * Demo LinuxKeyBoard
     *
     * @author hkq
     * @date 2019/05/01
     */
    public LinuxKeyBoard() {

        //添加监听调度员，如果是Swing程序，就用SwingDispatchService，不是就用默认的
        GlobalScreen.setEventDispatcher(new DefaultDispatchService ());
        try {
            //注册监听
            GlobalScreen.registerNativeHook();
            //加入键盘监听，
            GlobalScreen.addNativeKeyListener(this);
        } catch (NativeHookException e) {
            e.printStackTrace();
        }
    }



    /***
     *
     * @param nativeEvent
     * 实现键盘监听的三个方法，根据自己情况实现
     */
    @Override
    public void nativeKeyTyped(NativeKeyEvent nativeEvent) {

    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent nativeEvent) {
        

    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nativeEvent) {
    }
}

```

简单明了。。。

有兴趣看看如何注册热键监听键盘<https://www.cnblogs.com/plumsq/p/10709122.html>
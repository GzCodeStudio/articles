---
title: Java环境搭建一个小型网页
date: 2019-04-30 19:34:00
categories:
	- [java]
tag:
	- 网页
	- Linux
author: 洪凯庆
---

Java环境搭建一个小型网页

<!--more-->

## 前言

1.我们学习编程的，无论你是在什么学历的时候，无论你入门学习的是那种编程语言，都有一种想把自己编程做的东西呈现给别人看看。

2.我这个只是一个云服务器中搭建Java环境+Tomcat，在Tomcat的webapps文件上传一个网页，并映射端口为：8080，游览器输入云服务器公网IP+：8080/项目名称，即可访问

3.效果网址：<http://123.207.25.57:8080/zpx/175.html>

## 准备工作

1.首先你得有一个云服务器，可以在百度搜索“腾讯云”/“阿里云”进入官网自行购买，现在的学生服务器也就9块多一个月吧，这种教程就不给了，自行百度就好了，系统最好是Linux的，那个版本你随意啦。

2.如果你是一个小白，那么请不要害怕，针对这个教程的操作是在云服务器上的，但是云服务器也不过是一台Linux系统的远程电脑而已。

3.如果你已经购买了云服务器并在云服务器的官网登录，请进入控制台，点击安全组开通22端口，8080端口，具体教程请自行百度吧

## 开始

1.假设你开通了自己所购买的云服务器的22端口，那你用Xshell连接的界面如下：

```
*** System restart required ***
Last login: Sun Dec 30 15:37:23 2018 from 219.137.26.162
root@VM-0-14-ubuntu:~#
```

2.打开xftp将下载好的jdk、Tomcat包上传到服务器的/usr/local/packages文件夹下，如果你服务器没有这个文件夹，可以先新建一下，命令如下oot># ./configure
WARNING: C++ compiler too old, need g++ 4.8 or clang++ 3.4 (CXX=g++)
creating  ./icu_config.gypi
{ 'target_defaults': { 'cflags': [],
                       'default_configuration': 'Release',
                       'defines': [],
                       'include_dirs': [],
                       'libraries': []},
  'variables': { 'asan': 0,
                 'gas_version': '2.20',
                 'host_arch': 'x64',
                 'icu_small': 'false',
                 'node_byteorder': 'little',
                 'node_install_npm': 'true',
                 'node_prefix': '/usr/local',
                 'node_release_urlbase': '',
                 'node_shared_http_parser': 'false',

```
mkdir /usr/local/packages
```

3.如果你不知道从哪里下载jdk、tomcat的云服务器解压包，可以用我的，链接:链接：<https://pan.baidu.com/s/1Fg2ueP8HMXlsV2h4IcpaFg> 提取码：wrqu

## 上传完毕后，当然得解压啦。

### 安装jdk

1.jdk得解压在一个命名为Java的文件夹下，先在云服务器上创建这样一个文件夹，再解压：

```
mkdir /usr/java
cd    /usr/java  
tar -zxvf  jdk-8u191-linux-x64.tar.gz
```

2.配置环境变量

输入：vim /etc/profile,添加如下代码：

```
export JAVA_HOME=/usr/java/jdk1.8.0_191  
export JRE_HOME=/usr/java/jdk1.8.0_191 /jre  
export CLASSPATH=.:$JAVA_HOME/lib:$JRE_HOME/lib:$CLASSPATH  
export PATH=$JAVA_HOME/bin:$JRE_HOME/bin:$JAVA_HOME:$PATH
```

保存后执行：

```
source /etc/profile
```

验证安装：输入:

```
java -version
```

出现如下，说明安装成功

```
root@VM-0-14-ubuntu:~# java -version
java version "1.8.0_191"
Java(TM) SE Runtime Environment (build 1.8.0_191-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.191-b12, mixed mode)
```

### 安装Tomcat

1.tomcat得解压在一个命名为Tomcat的文件夹下，先在云服务器上创建这样一个文件夹，再解压

```
mkdir /usr/Tomcat
cd    /usr/Tomcat
tar -zxvf apache-tomcat-8.5.33.tar.gz
```

此时Tomcat文件夹下生成一个apache-tomcat-8.5.33文件夹

2、配置环境

进入到上面的apache-tomcat-8.5.33的bin文件夹下：

```
cd /usr/Tomcat/apache-tomcat-8.5.33/bin
```

写入配置

```
vim setclasspath.sh
```

在文件的最后写入

```
export JAVA_HOME=/usr/java/jdk1.8.0_191
export JRE_HOME=/usr/java/jdk1.8.0_191/jre
```

保存后验证

在cd /usr/Tomcat/apache-tomcat-8.5.33/bin 目录下执行

```
sh startup.sh
```

输出如下则是成功开启tomcat:

```
root@VM-0-14-ubuntu:/usr/Tomcat/apache-tomcat-8.5.33/bin# sh startup.sh
Using CATALINA_BASE:   /usr/Tomcat/apache-tomcat-8.5.33
Using CATALINA_HOME:   /usr/Tomcat/apache-tomcat-8.5.33
Using CATALINA_TMPDIR: /usr/Tomcat/apache-tomcat-8.5.33/temp
Using JRE_HOME:        /usr/java/jdk1.8.0_191/jre
Using CLASSPATH:       /usr/Tomcat/apache-tomcat-8.5.33/bin/bootstrap.jar:/usr/Tomcat/apache-tomcat-8.5.33/bin/tomcat-juli.jar
Tomcat started.
```

配置防火墙

```
vim /etc/sysconfig/iptables
```

写入：开通8080端口

```
A INPUT -m state --state NEW -m tcp -p tcp --dport 8080 -j ACCEPT
```

就是配置完iptables之后不要忙着重启要先保存的服务，然后再重启

```
service iptables save
service iptables restart
```

3、开放云服务器的8080端口
正常来说，此时通过 服务器外网ip:8080 就能访问到tomcat网页，但是我在第一次startup.sh成功后始终显示连接超时，浏览器提示检查防火墙等，

实际上需要手动开通云服务器的外网端口，即：进入阿里云控制台——实例——安全组——添加安全组规则,具体教程请自行百度。

## 上传网页，并访问

1.我们已经搭建好云服务器上的java环境和tomcat环境了，但是怎样才能让别人看看我们这个服务器上，有没有好玩的东西呢？所以现在我们找点好玩好看的网页上传一下吧，然后让小伙伴们访问一下。

2.我今天就不在这里写网站了，我用一下以前捣鼓的代码试试玩一下，源码链接：链接：<https://pan.baidu.com/s/1jApBHqpaa2w1mLt4uVsghw> 提取码：pghr

## 开始

打开Xftp上传刚刚在百度云盘下载的代码文件夹到云服务器的/usr/Tomcat/apache-tomcat-8.5.33/webapps下，然后进入/usr/Tomcat/apache-tomcat-8.5.33/bin 目录下执行

```
sh shurdown.sh
sh startup.sh
```

即可访问了


---
title: Java反射-静态/动态加载类
date: 2019-05-28 18:50:00
categories:
	- [java]
tag:
	- Java
	- 反射
author: 洪凯庆
---

Java反射-静态/动态加载类

<!--more-->

​		Class 类是对象,是java.lang.Class类的实例对象。任何一个类都是Class的实例对象，这个实例对象有三种表示方式:

第一种表示方式：实际在告诉我们任何一个类都有一个隐含的静态成员变量class.

第二种表达方式，已经知道该类的对象通过getClass方法

第三种表示方式。。。自行百度

```java
FOO foo1=new Foo();
//第一种表示方式
Class c1=Foo.class;
//第二种表达方式
Class c2=fool.getClass();

```

​		mmp...看完这个介绍，我瞬间对反射这个概念心死了。。。什么玩意啊？百度、谷歌搜索了相关资料看了看，发现尽管“反射”这个词语描述得现当抽象，但它描述的是底层的概念了。百度描述：

​		要正确使用Java反射机制就得使用java.lang.Class这个类。它是Java反射机制的起源。当一个类被加载以后，Java虚拟机就会自动产生一个Class对象。通过这个Class对象我们就能获得加载到虚拟机当中这个Class对象对应的方法、成员以及构造方法的声明和定义等信息。

 **反射API**

​        **反射API用于反应在当前Java虚拟机中的类、接口或者对象信息**

​		**功能—获取一个对象的类信息.**

​       **—获取一个类的访问修饰符、成员、方法、构造方法以及超类的信息.**

​       **—检获属于一个接口的常量和方法声明.**

​       **—创建一个直到程序运行期间才知道名字的类的实例.**

​       **—获取并设置一个对象的成员，甚至这个成员的名字是   在程序运行期间才知道.**

​       **—检测一个在运行期间才知道名字的对象的方法**

 利用Java反射机制我们可以很灵活的对已经加载到Java虚拟机当中的类信息进行检测。当然这种检测在对运行的性能上会有些减弱，所以什么时候使用反射，就要靠业务的需求、大小，以及经验的积累来决定。

​		所以，你看懂了吗？？？我理解了一点点，写出来试试，主要是怕忘了；不知道你有没有接触到框架了？例如：spring、springMVC......这些框架使用时为什么如此优雅啊？正如Java 反射介绍一般：学会Java 反射机制就能更好理解框架的使用......以前，我是不信的！！！因为个人的原因有用过springMVc等框架做点不入流的小网站，当时用的时候真的是照别人的源码改的。底层实现原理，我真的是一点也不懂，边学Java反射时，我也在不停反思......框架真的只是把东西封装一下吗？我感觉不是的，但我现在说不上来，我们现在还是静下心学学Java 反射。

当我们为了方便使用高级编程工具eclipse、idea写代码时，恰恰就忽略了关注Java代码编译时、运行时的状态，所以先暂时回归记事本、命令行编译代码的时代，我们一起聊聊Java反射；我们先在代码实现一个WPS软件，它有world、Excel等功能：

```java
package reflexJava;

public class wps {
public static void main(String[] args) {
    //new 创建对象 是静态加载类
    	if ("World".equals(args[0])) {
		World world= new World();
		world.start();
	}
	if ("Excel".equals(args[0])) {
		Excel excel=new Excel();
		excel.start();
		
	}
	
}
```

代码非常简单不是，如果你是用eclipse、idea敲完这个代码早就报错了，它会提示你World类，Excel类还没有创建...cmd命令行编译也在报同样的错误；那我先把world类，Excel类先创建一下，方法也一并创建了，代码如下：

World类

```Java
package reflexJava;

public class World  {
	public void start() {
		System.out.print("world....start..");
	}

}
```

Excel类：

```java
package reflexJava;

public class Excel {
public void start() {
	System.out.println("excel...start..");
	
}
}

```

这样以后运行会不会报错呢？你先试试，这就是静态加载类，所有功能编译通过了，才能运行。但是一个软件有很多功能的，一个wps软件除了world、Excel，还有PPT等功能呢，如果都要所有功能编译好了才能运行，那我还没有写好PPT功能，是不是连world、Excel等功能都用不了。能不能我运行那个功能就加载那个？不用就不加载？这就是动态加载类的概念：

class 类其中一个方法Class.forName("类的全称")；不仅表示了类的类类型，还代表了动态加载类。

首先我们区分一下编译编译时刻、运行时刻：

编译时刻加载类是静态加载类；运行时刻加载类是动态加载类；

概念了解一下，接下来我们实行一下这个动态加载类，新建wpsBetter类代码如下：

```java
package reflexJava;

public class wpsBetter {
public static void main(String[] args) {
	try {
        //动态加载类，在运行时刻加载
		Class class1=Class.forName(args[0]);
		WpsAble wpsable=(WpsAble) class1.newInstance();
		WpsAble.start();
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (InstantiationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IllegalAccessException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
}
```

WpsAble类是一个接口标准类，定义world、Excel等功能标准接口类，代码如下：

```java
package reflexJava;
interface WpsAble {
	public void start() ;
	
	

}

```

当然world类，Excel类得implements WpsAble接口，代码如下：

World类：

```Java
package reflexJava;

public class World implements WpsAble {
	public void start() {
		System.out.print("world....start..");
	}

}

```

Excel类：

```java
package reflexJava;

public class Excel implements WpsAble {
public void start() {
	System.out.println("excel...start..");
	
}
}

```

我们命令行试试效果吧，代码如下：

```shell
c:\hkq>javac World.java
c:\hkq>javac Excel.java
c:\hkq> javac wpsBetter.java
c:\hkq> java wpsBetter World
c:\hkq> java wpsBetter Excel
```

Java 反射机制中动态加载类可以实现运行时加载，举个栗子：QQ、游戏等升级就是通过动态加载实现的，它不可能从新编译，从新编译意味着卸载再安装。。。未完待续。。。。。
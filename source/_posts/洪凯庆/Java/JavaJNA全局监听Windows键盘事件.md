---
title: Java中使用JNA实现全局监听Windows键盘事件
date: 2019-05-02 21:55:00
categories:
	- [java]
tag:
	- Java
	- JNA
author: 洪凯庆
---

Java中使用JNA实现全局监听Windows键盘事件

<!--more-->

### 前言：

​		一直打算做一个键盘音效软件，我就只会Java语言，其他语言有实现方式了，例如：c/c++、Python......等等，都有实现监听pc端键盘全局事件的方式。Java语言的实现方式真的找了很久资料，也问了很多人。。。百度上一搜索都是建议用jni让Java跟c/c++语言结合实现这个功能，或者用jna让Java跟c/c++语言结合实现这个功能.....我就是不会c语言啊！！！超级无敌理直气壮的，哼╭(╯^╰)╮

其中一度想到了，要不我就放弃吧，乖乖学C/C++...最后，没错，我实现了，哈哈哈

### 功能剖析：

Java代码运行在Jvm虚拟机内，键盘输入的东西，只有操作系统知道，Jvm虚拟机如何知道呢？那就是JNI编程，通过写C/C++代码，监听操作系统的的输入流，然后通过JNI调用。虽然我不会JNI，也不会C/C++,但幸运的是，SUN公司已经实现了这个代码，弄出一个叫JNA的东西（Java Native Access）,给Java提供了访问操作系统键盘鼠标的能力。

然后将人家的代码完整拷贝，想跑一下，结果没jar包，一直报错，根据包名百度，在maven仓库中找相关jar包，（想找官方的jar包和一些文档，无奈，因为被收购的原因，有些链接已经挂了，找不到哇）找到几个，放进去，编译不报错了，运行一直报错，换了好几个jar包，还是不行，真是可郁闷了。最后在一个国内的仓库网站找到一个清晰的分类，下载里面的大分类下面的一组jar包，运行成功了。网址是www.mvnjar.com。

### 实现过程：

下面贴上代码，感觉只是为了实现功能，代码写得很Low。运行需要的jar包在仓库下，网址是：https://www.mvnjar.com/net.java.dev.jna/list.html，下载jna-5.2.0.jar和jna-platform-5.2.0.jar这两个就可以

```java
package Soundeffects.hkq;

import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinUser;

/***
 *  Demo WinKeyBoard
 * windows系统下实现全局键盘的监听，基于lib目录下的jna-4.0.0jar和jna-platform.jar
 * 我也不明白为啥这个类我改成线程了
 * @author hkq
 */

public class WinKeyBoard implements Runnable{
    private static WinUser.HHOOK hhk;
    private static WinUser.LowLevelKeyboardProc keyboardHook;
    final static User32 lib = User32.INSTANCE;
    private boolean [] on_off=null;
    public WinKeyBoard(boolean [] on_off){
        this.on_off = on_off;
    }

    @Override
    public void run() {
        WinDef.HMODULE hMod = Kernel32.INSTANCE.GetModuleHandle(null);

        keyboardHook = new WinUser.LowLevelKeyboardProc () {
            public WinDef.LRESULT callback(int nCode, WinDef.WPARAM wParam, WinUser.KBDLLHOOKSTRUCT info) {

               System.out.println("运行成功，控制台输出");

                return lib.CallNextHookEx(hhk, nCode, wParam, info.getPointer());
            }
        };
        hhk = lib.SetWindowsHookEx(User32.WH_KEYBOARD_LL, keyboardHook, hMod, 0);
        int result;
        WinUser.MSG msg = new WinUser.MSG ();
        while ((result = lib.GetMessage(msg, null, 0, 0)) != 0) {

        }
        lib.UnhookWindowsHookEx(hhk);
    }
}

```

System.out.println("运行成功，控制台输出");这个位置的代码可以替换你获取键盘事件后的操作啊

bil bil bil bil
$ git config -l	// 查看配置
$ git config --system --list	// 查看系统配置
$ git config --global --list	// 查看用户(本地)配置
全局设置
$ git config --global user.name "游戏疯12138"
$ git config --global user.email "2686958791@qq.com"


$ git init		// 在当前文件夹里创建 .git 文件夹

$ git clone 链接	// 克隆/复制别人的库

$ git add  .	// 添加到缓存区
$ git commit -m "信息"	// 提交到本地仓库 -m 后面加备注
$ git push		// 提交到远程仓库

$ git status 文件名



忽略文件	// 不会被提交上去
文件里的空号和#不会被提交上去
*.xxx		// 忽略所有的 .xxx 文件
!XXX.xxx		// 忽略 XXX.xxx 文件 
/xxx		// 除了 xxx 文件夹其他都忽略
xxx/		// 忽略 xxx 文件夹
xxx/*.XXX		// 忽略xxx/*.XXX 文件




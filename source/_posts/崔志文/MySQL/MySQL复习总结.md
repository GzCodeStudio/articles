---
title: MySQL复习总结
date: 2019-06-24 14:57:05
categories:
	- [MySQL]
tag:
	- MySQL
author: 崔志文
---

又是一个期末啦，最不喜欢的MySQL要考试啦

这是上课时候的不完整总结

<!--more-->

#### MySQL考试题型：

> ​	**1）填空题 每空2分共20分**
> ​	**2）单选题 每题2分共20分或30分**
> ​	**3）判断题 每题1分共十分**
> ​	**4）问答题 共八道40-50分，分值从3-10分不等**
> ​	**重点：以下题目都要SQL语句实现**
> ​		**如何创建数据库？**
> ​		**如何创建用户，并且赋予响应的权限？**
> ​		**如何创要求创建一张表？**
> ​		**如何向表中插入数据？**
> ​		**日期函数的用法：date_add()函数**
> ​		**如何定义一个存储过程并调用它？**
>
> ​		**分组查询 GROUP BY HAVING**

#### 分章节复习：

```sql
-- 创建数据库
CREATE DATABASE 数据库名;

-- 进入数据库
USE studb;

-- 修改数据库：
ALTER DATABASE 数据库的参数;

-- 删除数据库：
DROP DATABASE 数据库名;

-- 删除表：
DROP TABLE IF EXISTS 表名;

-- 删除视图：
DROP VIEW 视图名;

-- 删除索引:
DROP INDEX 索引的名字 ON 表名;	

-- 删除学生信息表中ID为1的数据行：
DELETE FROM 学生信息表 WHERE ID = 1;

-- 删除约束（它的本质是改变表的结构）：
ALTER TABLE 表名 DROP PRIMARY KEY;

-- 删除列（它的本质也是改变表的结构）：
ALTER TABLE 表名 DROP COLUMN 列名;

-- 删除存储过程：
DROP PROCEDURE 存储过程的名字;
DROP PROCEDURE IF EXISTS 存储过程的名字;

-- 删除函数：
DROP FUNCTION 函数名;
DROP FUNCTION IF EXISTS 函数名;

-- 删除触发器：
DROP TRIGGER 触发器名字;

-- 3.2）表Table
-- 表结构是根据列定义的。
-- 列：列名和数据类型
CREATE TABLE stuInfo
(
	stuid INTEGER PRIMARY KEY AUTO_INCREMENT,
	stuname VARCHAR(20) NOT NULL COMMENT '学生姓名',
	address VARCHAR(20),
	birthday DATE,
	...
)ENGINE=InnoDB;

-- 修改表
ALTER TABLE 表名;

-- 重点：
-- 针对一张表的操作

	-- 增加数据：
	INSERT INTO stuInfo (列名1, 列名2, 列名3, ...)
		VALUE(‘值1’, '值2', '值3', ...);

	-- 删除数据：
	DELETE FROM stuInfo WHERE id = 1；
	DELETE FROM 表名;
	
	-- 查询SELECT：
	SELECT * FROM 表名 WHERE 条件;
	SELECT 列名1, 列名2, 列名3, ... FROM 表名 WHERE 条件;
	
	-- 单表查询：
	SELECT * FROM 表名
		WHERE 条件 GROUP BY HAVING
		ORDER BY
		LIMIT;
		
	-- 多表查询：
	-- 表的连接
	-- 分为内连接和外连接
	INNER JOIN
	
	-- 更新数据：
	-- 更新一个列
	UPDATE shuInfo SET 列名=新值;
	-- 更新多个列：
	UPDATE stuInfo SET 列名1=新值1, 列名2=新值2, 列名3=新值3, ...;
```

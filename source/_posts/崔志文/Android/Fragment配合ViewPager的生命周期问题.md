---
title: ViewPager配合Fragment的生命周期问题
date: 2019-05-08 02:10:05
categories:
	- [Android]
tag:
	- Android
author: 崔志文
---

> 本来想好好学Servlet的，结果因为比赛要做Android项目，那只能捣鼓安卓了。今天记一章在使用ViewPager配合Fragment时遇到的生命周期问题。

<!--more-->

## Fragment生命周期

探究生命周期之前先了解下Fragment的生命周期

> **onAttach**：onAttach()在fragment与Activity关联之后调调查用。需要注意的是，初始化fragment参数可以从getArguments()获得，但是，当Fragment附加到Activity之后，就无法再调用setArguments()。所以除了在最开始时，其它时间都无法向初始化参数添加内容。

> **onCreate**：fragment初次创建时调用。尽管它看起来像是Activity的OnCreate()函数，但这个只是用来创建Fragment的。此时的Activity还没有创建完成，因为我们的Fragment也是Activity创建的一部分。所以如果你想在这里使用Activity中的一些资源，将会获取不到。比如：获取同一个Activity中其它Frament的控件实例。(代码如下：)，如果想要获得Activity相关联的资源，必须在onActivityCreated中获取。

> **onCreateView**：在这个fragment构造它的用户接口视图(即布局)时调用。

> **onActivityCreated**：在Activity的OnCreate()结束后，会调用此方法。所以到这里的时候，Activity已经创建完成！在这个函数中才可以使用Activity的所有资源。

> **onStart**：当到OnStart()时，Fragment对用户就是可见的了。但用户还未开始与Fragment交互。在生命周期中也可以看到Fragment的OnStart()过程与Activity的OnStart()过程是绑定的。意义即是一样的。以前你写在Activity的OnStart()中来处理的代码，用Fragment来实现时，依然可以放在OnStart()中来处理。

> **onResume**：当这个fragment对用户可见并且正在运行时调用。这是Fragment与用户交互之前的最后一个回调。从生命周期对比中，可以看到，Fragment的OnResume与Activity的OnResume是相互绑定的，意义是一样的。它依赖于包含它的activity的Activity.onResume。当OnResume()结束后，就可以正式与用户交互了。

> **onPause**：此回调与Activity的OnPause()相绑定，与Activity的OnPause()意义一样。

> **onStop**：这个回调与Activity的OnStop()相绑定，意义一样。已停止的Fragment可以直接返回到OnStart()回调，然后调用OnResume()。

> **onDestroyView**：如果Fragment即将被结束或保存，那么撤销方向上的下一个回调将是onDestoryView()。会将在onCreateView创建的视图与这个fragment分离。下次这个fragment若要显示，那么将会创建新视图。这会在onStop之后和onDestroy之前调用。这个方法的调用同onCreateView是否返回非null视图无关。它会潜在的在这个视图状态被保存之后以及它被它的父视图回收之前调用。

> **onDestroy**：当这个fragment不再使用时调用。需要注意的是，它即使经过了onDestroy()阶段，但仍然能从Activity中找到，因为它还没有Detach。

> **onDetach**：Fragment生命周期中最后一个回调是onDetach()。调用它以后，Fragment就不再与Activity相绑定，它也不再拥有视图层次结构，它的所有资源都将被释放。



## Fragment结合ViewPager生命周期

本来想在onCreateView中加入一个滑到相应布局然后更改title为对应的碎片名，结果总是显示下一个碎片的名字，如图：

![](http://pqxmmkwr0.bkt.clouddn.com/onCreateView.png)

运行时的截图：

![](http://pqxmmkwr0.bkt.clouddn.com/%E6%88%AA%E5%9B%BEgif01.gif)

什么情况？？试图创建完成时设置title不对？那就log打印下启动时的生命周期变化：

![](http://pqxmmkwr0.bkt.clouddn.com/fragment%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F%E6%88%AA%E5%9B%BE.png)

What？？？当前碎片不是主页吗，第二个碎片”分类“也加载了视图？

看这个方法设置标题不太妥，那就换个思路，在Activity中对ViewPager的选中进行监听，代码如下

```java
private List<String> mTitles = Arrays.asList("主页", "分类", "笔记", "我");
mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
           @Override
           public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

           }

           @Override
           public void onPageSelected(int position) {
                setTitle(mTitles.get(position));
           }

           @Override
           public void onPageScrollStateChanged(int state) {

           }
       });
```

ok，这个问题解决了，那就对ViewPager+Fragment生命周期继续进行探究

那么切换到下一个碎片时候会怎么变化呢

主页-->分类

![](http://pqxmmkwr0.bkt.clouddn.com/%E4%B8%BB%E9%A1%B5%E5%88%87%E6%8D%A2%E5%88%B0%E5%88%86%E7%B1%BB.png)

分类-->笔记

![](http://pqxmmkwr0.bkt.clouddn.com/%E5%88%86%E7%B1%BB%E5%88%87%E6%8D%A2%E5%88%B0%E7%AC%94%E8%AE%B0.png)

笔记-->我

![](http://pqxmmkwr0.bkt.clouddn.com/%E7%AC%94%E8%AE%B0%E5%88%87%E6%8D%A2%E5%88%B0%E6%88%91.png)



## 总结

在除了首尾碎片时，ViewPager会对下一个碎片进行视图预加载，如果用x的值来代表位置，当前碎片的值为0，那切换到当前碎片时会对-2视图进行销毁，就是这么个规律。

好啦这一节结束啦，用师兄的话说就是今天又学到了好多知识，好开心！！！
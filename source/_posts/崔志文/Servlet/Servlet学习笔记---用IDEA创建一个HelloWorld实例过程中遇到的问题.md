---
title: Servlet学习笔记---用IDEA创建一个HelloWorld实例过程中遇到的问题
date: 2019-05-01 16:21:15
categories:
	- [Servlet]
tag:
	- Servlet
author: 崔志文
---

> 习惯了Android Studio操作，所以我选择用Intellij IDEA来创建一个Servlet实例。磕磕绊绊总算成功运行了一个HelloWorld，下面记录下我在这个过程中遇到的问题

<!--more-->

## 1.Tomcat控制台输出一大堆乱码

![](http://lc-uoULb5IA.cn-n1.lcfile.com/aae5ecab245c041698cb/Tomcat%E4%B9%B1%E7%A0%81.png)

### 这种问题一般都是编码问题，解决方法：

第一步，进入设置找到如图示的地方，修改两个圈起来的地方

![](http://lc-uoULb5IA.cn-n1.lcfile.com/b9c0c7e10c1e25a5ad9a/IDEA%E7%BC%96%E7%A0%81.png)

第二步，找到IDEA安装目录下的bin目录用文本编辑器修改这两个文件，添加一行：`-Dfile.encoding=UTF-8`

![](http://lc-uoULb5IA.cn-n1.lcfile.com/b692455b6b7f660f4ffb/IDEA%E5%AE%89%E8%A3%85%E7%9B%AE%E5%BD%95bin.png)

**重启IDEA，问题解决，如图**

![](http://lc-uoULb5IA.cn-n1.lcfile.com/e8bd31574a19540c4739/%E6%AD%A3%E5%B8%B8%E6%98%BE%E7%A4%BA%E7%9A%84%E6%8E%A7%E5%88%B6%E5%8F%B0.png)

## 2.运行项目HTTP Status 405 ? Method Not Allowed

![](http://lc-uoULb5IA.cn-n1.lcfile.com/c5b7bca04e90e9511e74/%E8%BF%90%E8%A1%8C%E9%A1%B9%E7%9B%AE%E5%87%BA%E7%8E%B0405.png)

方法不被允许？？？？？

我的代码是：

```java
public class HelloWorld extends HttpServlet {

    private String message;

    @Override
    public void init() {
        message = "Hello GzCodeStudio!";
        System.out.println("-------------------------运行成功------------------------");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        super.doGet(request, response);
        // 设置响应内容类型
        response.setContentType("text/html");

        // 实际的逻辑是在这里
        PrintWriter out = response.getWriter();
        out.println("<h1>" + message + "</h1>");
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
```



好像没什么问题啊，摸不着头脑就找百度，最好的老师

原因是**调用了父类HttpServlet**，那直接去掉doGet中的`super.doGet(request, response);`就可以啦

磕磕绊绊终于运行成功啦

![](http://lc-uoULb5IA.cn-n1.lcfile.com/cb5b67c44366e91fa60e/HelloWorld%E8%BF%90%E8%A1%8C%E6%88%90%E5%8A%9F.png)

通过创建这个实例，我了解到了很多，包括Servlet的项目结构等等，总体来说收获很大，很Nice！！！
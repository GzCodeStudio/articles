---
title: 文章收藏
date: 2019-04-27 00:00:00
categories:
	- [收藏]
author: 董海镔
---



收藏一些不错的文章

<!--more-->

# JAVA

[细思极恐-你真的会写java吗? ](http://lrwinx.github.io/2017/03/04/细思极恐-你真的会写java吗/)

[跟上 Java 8 – Stream API 快速入门](http://www.importnew.com/26090.html)

[跟上 Java 8 – 你忽略了的新特性](http://www.importnew.com/26144.html)

[跟我学Dubbo系列之Java SPI机制简介](https://www.jianshu.com/p/46aa69643c97)

[JDK8·获取方法的参数名](https://docs.oracle.com/javase/tutorial/reflect/member/methodparameterreflection.html)

[Java 8 中的 Streams API 详解](https://www.ibm.com/developerworks/cn/java/j-lo-java8streamapi/)

[java8 stream流操作的flatMap（流的扁平化）](https://blog.csdn.net/Mark_Chao/article/details/80810030)

[详解Java8 Collect收集Stream的方法](https://www.jb51.net/article/138519.htm)

[Java 8 习惯用语（一）·Java 中的一种更轻松的函数式编程途径](https://www.ibm.com/developerworks/cn/java/j-java8idioms1/index.html)

[Java 8 习惯用语（二）·函数组合与集合管道模式](https://www.ibm.com/developerworks/cn/java/j-java8idioms2/index.html)

[Java 8 习惯用语（三）·传统 for 循环的函数式替代方案](https://www.ibm.com/developerworks/cn/java/j-java8idioms3/index.html)

[Java 8 习惯用语（四）·提倡使用有帮助的编码](https://www.ibm.com/developerworks/cn/java/j-java8idioms4/index.html)

[Java 8 习惯用语（五）·传递表达式（pass-through lambdas）的替代方案](https://www.ibm.com/developerworks/cn/java/j-java8idioms5/index.html)

[Java 8 习惯用语（六）·为什么完美的 lambda 表达式只有一行](http://www.ibm.com/developerworks/cn/java/j-java8idioms6/index.html)

[Java 8 习惯用语（七）·函数接口](https://www.ibm.com/developerworks/cn/java/j-java8idioms7/index.html)

[Java 8 习惯用语（八）·JAVA知道你的类型](https://www.ibm.com/developerworks/cn/java/j-java8idioms8/index.html)

[Java 8 习惯用语（九）·级联 lambda 表达式](https://www.ibm.com/developerworks/cn/java/j-java8idioms9/index.html)

[Java 8 习惯用语（十）·使用闭包捕获状态](https://www.ibm.com/developerworks/cn/java/j-java8idioms10/index.html)

[Java 泛型 <? super T> 中 super 怎么 理解？与 extends 有何不同？](https://www.zhihu.com/question/20400700)

[Java8日期和时间API](https://blog.csdn.net/a80596890555/article/details/58687444)

[Java 8 故障排查指南](https://docs.oracle.com/javase/8/docs/technotes/guides/troubleshoot/)

[后端架构师技术图谱](https://github.com/xingshaocheng/architect-awesome)

## 源码

[mac下编译openjdk1.9及集成clion动态调试](https://www.jianshu.com/p/ee7e9176632c)

## 并发

[全面理解Java内存模型(JMM)及volatile关键字](https://blog.csdn.net/javazejian/article/details/72772461)

[深入理解Java并发之synchronized实现原理](https://blog.csdn.net/javazejian/article/details/72828483)

[Java并发编程：volatile关键字解析](http://www.importnew.com/18126.html)

[一道非常棘手的 Java 面试题：i++ 是线程安全的吗？](https://mp.weixin.qq.com/s?__biz=MzI3ODcxMzQzMw==&mid=2247486263&idx=1&sn=74de5f37ed7cc9392ac1464bdac2a3ea&chksm=eb538e01dc240717f974e723654bc007502e0e1aa97778a1169617f12b4105328a891ace6991&mpshare=1&scene=1&srcid=0721ExJRtaGATUFq5s7QFlys#rd)

[32位JVM对long类型的赋值不是原子性操作](https://blog.csdn.net/qq_25343557/article/details/78010832)

[非阻塞同步算法与CAS(Compare and Swap)无锁算法](https://www.cnblogs.com/Mainz/p/3546347.html)

[oracle文档Threads and Locks](https://docs.oracle.com/javase/specs/jls/se8/html/jls-17.html#jls-17.7)

[必须要理清的Java线程池（原创）](https://www.jianshu.com/p/50fffbf21b39)

[如何优雅的使用和理解线程池](https://crossoverjie.top/2018/07/29/java-senior/ThreadPool/?nsukey=dxEIxoDRGW%2F5xjALIiUcah6aQplj2v6ABSEC3WBqZRw28mTRhhW6IFcQcewjiA7oyxOqxtz4jFBsyWZMN%2B62WJf%2BgqItLwW48IzMMmnG6QU%2BdR%2FuI6EmWp%2FLIaoAm49x9ab9LIVmWiRNUbLaz8GBoQFQEyMl8NtwDArJa98lsEY1flQ19ZMEyNNXWTn4UhdO8Q3MQGrhpZs%2BnFi48CcVEA%3D%3D)

## Jvm

[JVM GC调优一则–增大Eden Space提高性能](https://www.cnblogs.com/firstdream/p/5763689.html)

[JAVA堆外内存](https://www.cnblogs.com/moonandstar08/p/5107648.html)

[不重启JVM，替换掉已经加载的类，偷天换日？](https://mp.weixin.qq.com/s/IA6nNxBEDpQ7HqDKkFU0mA)

## JNI

[Java Native Interface (JNI)](https://www3.ntu.edu.sg/home/ehchua/programming/java/JavaNativeInterface.html)

[IntelliJ IDEA平台下JNI编程（一）—HelloWorld篇](https://www.jianshu.com/p/44cbe11e5d35)

[IntelliJ IDEA平台下JNI编程（二）—类型映射](https://www.jianshu.com/p/3d483597d641)

[IntelliJ IDEA平台下JNI编程（三）—字符串、数组](https://www.jianshu.com/p/f216b482b90e)

[(IntelliJ IDEA平台下JNI编程（四）—本地C代码访问JAVA对象](https://www.jianshu.com/p/1231d31d672e)

[IntelliJ IDEA平台下JNI编程（五）—本地C代码创建Java对象及引用](https://www.jianshu.com/p/19a6c3577cab)

[Android JNI(一)——NDK与JNI基础](https://www.jianshu.com/p/87ce6f565d37)

[Android JNI学习(二)——实战JNI之“hello world”](https://www.jianshu.com/p/b4431ac22ec2)

[Android JNI学习(三)——Java与Native相互调用](https://www.jianshu.com/p/b71aeb4ed13d)

[Android JNI学习(四)——JNI的常用方法的中文API](https://www.jianshu.com/p/67081d9b0a9c)

## 设计模式

[你真的会写单例模式吗——Java实现](http://www.importnew.com/18872.html)

## Spring

[Spring Boot Starters](https://www.nosuchfield.com/2017/10/15/Spring-Boot-Starters/)

[Spring HATEOAS](http://spring.io/projects/spring-hateoas)

[SpringBoot集成自定义HandlerMethodArgumentResolver](https://www.cnblogs.com/yangzhilong/p/7605889.html)

[Spring事务传播行为详解](https://segmentfault.com/a/1190000013341344)

### Spring Security

[spring security动态配置url权限](https://segmentfault.com/a/1190000010672041)

[Spring Boot实战之Spring Security权限管理](https://www.jianshu.com/p/bcbbf16610fb)

### Spring Data Rest

[Spring Data Rest ResourceProcessor not applied on Projections](https://stackoverflow.com/questions/47048099/spring-data-rest-resourceprocessor-not-applied-on-projections)

[Define entity resource link in spring data rest](http://samchu.logdown.com/posts/2073658-define-entity-resource-link-in-spring-data-rest)

[spring-boot-data-rest](https://docs.spring.io/spring-data/rest/docs/3.1.2.RELEASE/reference/html/)

[使用 Spring Data Rest 创建HAL风格Restful接口](https://www.jianshu.com/p/84f2bbffb885)

[Deleting an association over REST in Spring Data REST+HATEOAS](https://stackoverflow.com/questions/28711439/deleting-an-association-over-rest-in-spring-data-resthateoas)

[Spring REST response is different in a custom controller](https://stackoverflow.com/questions/40922466/spring-rest-response-is-different-in-a-custom-controller/40930712)

[Spring hateos 异常国际化(自定义处理异常)](https://stackoverflow.com/questions/44738565/handling-custom-exceptions-i18n-with-spring-data-rest)

[How to correctly use PagedResourcesAssembler from Spring Data?](https://stackoverflow.com/questions/21346387/how-to-correctly-use-pagedresourcesassembler-from-spring-data)

[Different JSON output when using custom json serializer in Spring Data Rest](https://stackoverflow.com/questions/22613143/different-json-output-when-using-custom-json-serializer-in-spring-data-rest)

[Custom Jackson Deserialization](https://github.com/spring-projects/spring-data-rest/blob/master/src/main/asciidoc/custom-jackson-deserialization.adoc)

## Mybatis

[深入了解MyBatis参数](https://blog.csdn.net/isea533/article/details/44002219)

[Mybatis3官方中文文档](http://www.mybatis.org/mybatis-3/zh/index.html)

# MySql

[mysql 各数据类型的 大小及长度](https://www.cnblogs.com/ghjbk/p/6681470.html)

## Shiro

[SpringBoot+Shiro学习之数据库动态权限管理和Redis缓存](https://blog.csdn.net/qq_20954959/article/details/55260255)

[shiro 扩展之动态加载权限](https://www.jianshu.com/p/5de287239061)

[跟我学Shiro](http://jinnianshilongnian.iteye.com/blog/2049092)

## AST

[Android AOP 之 AST：抽象语法树](https://toutiao.io/posts/w05d57/preview)

[JCTree](https://houbb.github.io/2017/10/13/jctree)

[Java注解处理器](https://www.race604.com/annotation-processing/)

## Swagger

[swagger常用注解说明](https://www.cnblogs.com/hyl8218/p/8520994.html)

[swagger注释API详细说明](https://blog.csdn.net/xupeng874395012/article/details/68946676)

[Swagger使用快照支持Spring boot 2.1.0](https://github.com/springfox/springfox/issues/2298)

## 库

[random-beans自动生成Bean](https://github.com/benas/random-beans)

# Python 

[机器学习100天](https://github.com/MachineLearning100/100-Days-Of-ML-Code)

# Git

[git book](https://git-scm.com/book/zh/v2)

[Git 大全](https://gitee.com/all-about-git)

[learngitbranching](https://learngitbranching.js.org/)

[git commit 时 emoji 使用指南](https://www.jianshu.com/p/dd480882b483)

# Maven

[Maven依赖中的Scope、传递与隔断](https://blog.csdn.net/wy0123/article/details/79556564)

# IDE

[在Intellij IDEA中使用Debug](https://www.cnblogs.com/chiangchou/p/idea-debug.html)

[IntelliJ IDEA官方文档中文版](https://www.w3cschool.cn/intellij_idea_doc/)

# Linux

[mv 命令批量移动文件夹](https://blog.csdn.net/cocoin/article/details/70146313)

[Linux 开启swap](https://www.cnblogs.com/cc11001100/p/7803583.html)

# 前端

[Inline style to act as :hover in CSS](https://stackoverflow.com/questions/131653/inline-style-to-act-as-hover-in-css)

# 其它

[markdown的流程图、时序图、甘特图画法](https://www.jianshu.com/p/a9ff5a9cdb25)

[如何在Markdown中画流程图](https://www.jianshu.com/p/b421cc723da5)

# 社区

[ImportNew](http://www.importnew.com/)

[ibm·developerworks](https://www.ibm.com/developerworks/cn/topics/)

# 博客

[Hello SHEN](http://www.ciaoshen.com/)

[纯洁的微笑](http://www.ityouknow.com/)

[rednaxelafx](https://rednaxelafx.iteye.com/)
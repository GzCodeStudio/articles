---
title: Mysql远程链接
date: '2017/04/11 11:49'
updated: '2017/04/11 11:49'
categories:
  - mysql
author: 董海镔
---
Linux环境下远程连接Mysql
<!-- more -->

查一下你的MYSQL用户表里, 是否允许远程连接

##### 1、授权

```
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '密码' WITH GRANT OPTION;
```

##### 2、刷新

```
flush privileges
```

##### 3、修改/etc/mysql/my.cnf

找到bind-address = 127.0.0.1这一行，注释掉

##### 4、重启mysql服务


```
/etc/init.d/mysql restart
```

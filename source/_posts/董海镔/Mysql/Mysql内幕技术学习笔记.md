---
title: Mysql内幕技术学习笔记
date: '2017/11/08 18:03'
updated: '2017/11/08 18:03'
categories:
  - mysql
author: 董海镔
---
marked
<!-- more -->

# INSERT插入数据

> 有以下几种方式

-  一次性插入所有值
```
INSERT INTO tb_name VALUES(value1, value2,...);
```
- 对数据列进行赋值，先给出数据列的名字，再列出它的值

```
INSERT INTO tb_name (vol_name1, col_name2,...) VALUES(value1, value2,...); 
```
- 包含col_name = value 的SET子句对数据列赋值

```
INSERT INTO tb_name SET col_name1=value1, col_name2=value2,...;
```

# 执行sql文件

- 在终端执行
```
mysql db_name < sql_file
```
- 在mysql中执行

```
source sql_file
```
- LOAD LOCAL 

没有过

# 检索信息

- 显示表的所有信息

```
SELECT * FROM tb_name;
```
# IN()操作符

IN()操作符与WHERE在一起用时，像OR操作符相似
```
SELECT * FROM tb_name WHERE col_name IN (value1, value2,...);
```
# 运算符

- 算术运算符

运算符 | 含义
---|---
+ | 加法
- | 减法
* | 乘法
/ | 除法
DIV | 整数除法
% | 求余（整数除法后的余数）

- 比较运算符

运算符 | 含义
---|---
< | 小于
> | 大于
= | 等于
<=> | 等于（能够与NULL值进行对比）
<> 或 != | 不等于
>= | 不小于
<= | 不大于

- 逻辑运算符

运算符 | 含义
---|---
AND | 逻辑与
OR | 逻辑或
XOR | 逻辑异或
NOT | 逻辑非

# 排序

```
ORDER BY col_name ASC|DESC,...
```
排序类型|含义
---|---
ASC|升序
DESC|降序

- ORDER BY与IF()函数结合

```
IF(expr, true, false)
```
对表达式进行判断，返回相对应的值，然后ORDER BY根据值来排序，后面也可以加上ASC|DESC

# 限制个数

```
LIMIT 个数
```
# 跳过N个结果，显示M个

```
LIMIT N, M
```

# 随机抽取

```
ORDER BY RAND() LIMIT 1
```

# 对输出列进行命名和求值

- 数字格式化

```
FORMAT(X:real, D:int [, local:string])
```
- 合并字符串

```
CONCAT(str:string*)
```
- 设置别名

```
col_name AS alias
```
- 平方根

```
SQRT(X:real)
```

# 与日期相关的问题

比较常见的几种日期操作

- 按日期排序

```
ORDER BY date ASC|DESC
```

- 查找某个日期或者某个日期范围

```
SELECT * FROM tb_name WHERE col_date = '2017-11-09';

SELECT * FROM tb_name WHERE col_date >= '2017-11-09' AND col_date < '2017-11-07';
```

- 提取日期值中的年、月、日等组成部分

```
YEAR(date:date) # 提取日期中的年

MONTH(date:date) # 提取日期中的月

DAYOFMONTH(date:date) # 提取日期中的日
```

- 计算两个日期的时间范围

```

```

- 用一个日期加上或减去一个时间间隔以求出另一个日期
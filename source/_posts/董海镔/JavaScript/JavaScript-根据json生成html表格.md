---
title: '[JavaScript]根据json生成html表格'
tags:
  - json
  - html
categories:
  - JavaScript
date: 2018-10-23 17:02:36
author: 董海镔
---



之前公司有一个需求是：通过js来生成html。而且大部分都是生成表格，直接通过字符串拼接的话，代码的可复用性太低的，所以写了个通用的json转html表格的工具。

<!-- more -->

# 代码

```javascript
htmlKit = {
    _tags: [], html: [], 
    _createAttrs: function (attrs) {
        var attrStr = [];
        for (var key in attrs) {
            if (!attrs.hasOwnProperty(key)) continue;
            attrStr.push(key + "=" + attrs[key] + "")
        }
        return attrStr.join(" ")
    }, _createTag: function (tag, attrs, isStart) {
        if (isStart) {
            return "<" + tag + " " + this._createAttrs(attrs) + ">"
        } else {
            return "</" + tag + ">"
        }
    }, start: function (tag, attrs) {
        this._tags.push(tag);
        this.html.push(this._createTag(tag, attrs, true));
        return this;
    }, end: function () {
        this.html.push(this._createTag(this._tags.pop(), null, false));
        return this;
    }, tag: function (tag, attr, text) {
        this.html.push(this._createTag(tag, attr, true) + text + this._createTag(tag, null, false));
        return this;
    },
    create: function () {
        var t = this.html.join("");
        this.clear();
        return t;
    },
    clear: function () {
        this._tags = [];
        this.html = [];
    }
};

function json2Html(data) {
    var hk = htmlKit;
    hk.start("table", {"cellpadding": "10", "border": "1"});
    hk.start("thead");
    hk.start("tr");
    data["heads"].forEach(function (head) {
        hk.tag("th", {"bgcolor": "AntiqueWhite"}, head)
    });
    hk.end();
    hk.end();
    hk.start("tbody");
    data["data"].forEach(function (dataList, i) {
        dataList.forEach(function (_data) {
            hk.start("tr");
            data["dataKeys"][i].forEach(function (key) {
                var rowsAndCol = key.split(":");
                if (rowsAndCol.length === 1) {
                    hk.tag("td", null, _data[rowsAndCol[0]])
                } else if (rowsAndCol.length === 3) {
                    hk.tag("td", {"rowspan": rowsAndCol[0], "colspan": rowsAndCol[1]}, _data[rowsAndCol[2]])
                }
            });
            hk.end()
        })
    });
    hk.end();
    hk.end();
    return hk.create()
}
```

# 使用说明

## HtmlKit

htmlKit是创建html标签的工具

### 函数

| 函数名                | 作用                        | 例子                                                         |
| --------------------- | --------------------------- | ------------------------------------------------------------ |
| start (tag, attrs)    | 创建未封闭标签头            | `start("table", {"cellpadding": "10", "border": "1"})`，输出`<table cellpadding="10" border="1">` |
| end ()                | 创建上一个start函数的标签尾 | 上面执行了start("table")，再执行end()，输出`</table>`        |
| tag (tag, attr, text) | 创建封闭标签                | `tag("th", {"bgcolor": "AntiqueWhite"}, "hello")`，输出`<th bgcolor="AntiqueWhite">hello</th>` |

## json2Html

json转Html

例子：

```javascript
var data = [
    {
        "chinese": 80,
        "mathematics": 89,
        "english": 90
    }
];

var total = 0;
data.forEach(function (value) {
    for (key in value) {
        total += value[key];
    }
});

var htmlMetadata = {
    "heads": ["语文", "数学", "英语"],
    "dataKeys": [["chinese", "mathematics", "english"], ["text","1:2:total"]], // rowspan:colspan:value
    "data": [data, [{"text": "合计","total": total}]]
};

var html = json2Html(htmlMetadata);

console.info(html);
```

输出结果（结果为了好看，格式化了）：

```html
<table cellpadding=10 border=1>
    <thead>
    <tr>
        <th bgcolor=AntiqueWhite>语文</th>
        <th bgcolor=AntiqueWhite>数学</th>
        <th bgcolor=AntiqueWhite>英语</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>80</td>
        <td>89</td>
        <td>90</td>
    </tr>
    <tr>
        <td>合计</td>
        <td rowspan=1 colspan=2>259</td>
    </tr>
    </tbody>
</table>
```

效果：

<table cellpadding=10 border=1><thead ><tr ><th bgcolor=AntiqueWhite>语文</th><th bgcolor=AntiqueWhite>数学</th><th bgcolor=AntiqueWhite>英语</th></tr></thead><tbody ><tr ><td >80</td><td >89</td><td >90</td></tr><tr ><td >合计</td><td rowspan=1 colspan=2>259</td></tr></tbody></table>



文中若存在错误或有更好的方法，恳请指出，谢谢。
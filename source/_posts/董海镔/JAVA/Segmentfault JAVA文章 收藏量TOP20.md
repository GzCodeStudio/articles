---
title: Segmentfault JAVA文章 收藏量TOP20
date: '2018-08-29 00:23'
updated: '2018-08-29 00:23'
categories:
  - java
author: 董海镔
---



# 前言

从18号开始在sf写下第一篇文章（说是笔记还差不多），惊奇地收到有人收藏我的文章的消息，觉得有点开心。突然脑子抽到想爬下sf里JAVA标签下的文章有多少，哪篇被收藏最多，哪篇被点赞最多。。。现在和大家分享下，收藏量前20的文章，被那么多人收藏应该是篇值得看的文章。

| 数据   | 数目 |
| ------ | ---- |
| 总篇数 | 9543 |

**注：上面数据截止时间是2018-08-28 21:40:45，数据可能不太精准**

<!-- more -->

# 收藏 TOP20

1. [FPB 2.0：免费的计算机编程类中文书籍 2.0](https://segmentfault.com/a/1190000010791103) [2208]
2. [一个两年Java的面试总结](https://segmentfault.com/a/1190000013550405) [873]
3. [从放弃迅雷和IDM到自己开发下载工具](https://segmentfault.com/a/1190000012345156) [761]
4. [想进大厂？50个多线程面试题，你会多少？（一）](https://segmentfault.com/a/1190000013813740) [623]
5. [最全前端资源汇集](https://segmentfault.com/a/1190000004978770) [531]
6. [我的阿里之路+Java面经考点](https://segmentfault.com/a/1190000013695159) [427]
7. [看完这篇Linux基本的操作就会了](https://segmentfault.com/a/1190000014840829) [426]
8. [为Java程序员金三银四精心挑选的300余道Java面试题与答案](https://segmentfault.com/a/1190000013885634) [365]
9. [秒杀架构实践](https://segmentfault.com/a/1190000014785707) [290]
10. [线程池，这一篇或许就够了](https://segmentfault.com/a/1190000009098623) [284]
11. [SegmentFault 技术周刊 Vol.40 - 2018，来学习一门新的编程语言吧！](https://segmentfault.com/a/1190000012721273) [277]
12. [使用 OAuth 2 和 JWT 为微服务提供安全保障](https://segmentfault.com/a/1190000009164779) [258]
13. [2018最新后端开发人员的路线图](https://segmentfault.com/a/1190000015356803) [247]
14. [源码之下无秘密 ── 做最好的 Netty 源码分析教程](https://segmentfault.com/a/1190000007282628) [247]
15. [在数据库中存储一棵树，实现无限级分类](https://segmentfault.com/a/1190000014284076) [234]
16. [国外程序员整理的Java资源大全](https://segmentfault.com/a/1190000002471482) [225]
17. [微服务实战：从架构到发布（一）](https://segmentfault.com/a/1190000004634172) [221]
18. [彻底搞懂Java内存泄露](https://segmentfault.com/a/1190000012167093) [205]
19. [前后端完全分离之 API 设计](https://segmentfault.com/a/1190000002690813) [202]
20. [Spring Boot 学习资料收集](https://segmentfault.com/a/1190000008539153) [200]
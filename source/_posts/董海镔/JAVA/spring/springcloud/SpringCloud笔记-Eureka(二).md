---
title: SpringCloud笔记-Eureka(二)
date: '2019-05-07 23:59:59'
categories:
  - java
tags:
  - Spring cloud
author: 董海镔
---



## Eureka是什么？

eureka是Netflix的子模块，也是一个核心的模块，eureka中有两个组件，一个是EurekaServer（一个独立的项目），这个是用于定位服务以及实现中间层服务器的负载均衡和故障转移，另一个是EurekaClient，他的作用是让各个服务间交互，使得交互变得非常简单：只要通过服务标识符即可拿到服务。

<!--more-->

关系图：

![](http://lc-dnchthtq.cn-n1.lcfile.com/17ac9c19a9df09e0b609/Eureka%E5%85%B3%E7%B3%BB%E5%9B%BE.png)

EurekaClient都向EurekaServer注册，关于提供服务和获取服务是相对的，怎么说呢，就是鉴权模块也可以通过注册中（EurekaServer）取得用户模块提供得服务，用户也可以获取鉴权模块提供的服务。

## 与SpringCloud关系

Spring Cloud 封装了 Netflix 公司开发的 Eureka 模块来实现服务注册和发现(可以对比Zookeeper)。Eureka 采用了 C-S 的设计架构。Eureka Server 作为服务注册功能的服务器，它是服务注册中心。而系统中的其他微服务，使用 Eureka 的客户端连接到 Eureka Server并维持心跳连接。这样系统的维护人员就可以通过 Eureka Server 来监控系统中各个微服务是否正常运行。SpringCloud 的一些其他模块（比如Zuul）就可以通过 Eureka Server 来发现系统中的其他微服务，并执行相关的逻辑。

## 搭建EurekaServer

> learn-eureka

创建一个Spring Boot项目，引入Spring Cloud依赖

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-dependencies</artifactId>
        <!-- 当前最新版本是Greenwich.SR1 -->
        <version>Greenwich.SR1</version>
        <type>pom</type>
        <scope>import</scope>
    </dependency>
</dependencies>
```

### 修改配置

```yaml
server:
  # 访问端口
  port: 8761
eureka:
  server:
    # 关闭自我保护机制
    enable-self-preservation: false
    # 设置清理间隔时间（单位：毫秒 默认是60*1000）
    eviction-interval-timer-in-ms: 4000
  instance:
    hostname: localhost
  client:
    # 不把自己当作一个客户端注册到自己身上
    register-with-eureka: false
    # 不需要从服务端获取信息（因为在这里自己就是服务端，而且自己已经禁用注册）
    fetch-registry: false
    service-url:
      # 客户端注册地址
      default-zone: http://${eureka.instance.hostname}:${server.port}/eureka/

spring:
  application:
    # 名字，如果名字相同，Eureka会认为是同一个服务
    name: learn-eureka-server
```

### 启动类

> EurekaServerApplication.java

```java
/**
 * @author DHB
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServerApplication.class, args);
    }
}
```

到这一步，你没有看错，EurekaServer就搭建好了，我们启动看看。

访问http://localhost:8761/

看到下面这个界面

![](http://lc-dnchthtq.cn-n1.lcfile.com/abc7ffbc888a46261a2b/SpringCloud%E7%AC%94%E8%AE%B0%28%E4%BA%8C%29-EurekaServer.png)

当前什么服务都没有注册上来，下面我们模拟一个用户模块

## EurekaClient

> learn-user

创建一个用户模块模拟Eureka客户端，引入客户端依赖

```xml
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <!-- 这个是Eureka客户端依赖 -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
    </dependencies>
```

### 配置

> application.yml

```yaml
server:
  port: 5000
eureka:
  client:
    # 服务端注册地址
    service-url:
    # 这里有个坑，写default-zone是无效的
      defaultZone: http://localhost:8761/euerka/
  instance:
    # 此实例注册到eureka服务端的唯一的实例ID
    instance-id: user-client-1
    # 是否显示IP地址
    prefer-ip-address: true
    # eureka客户端需要多长时间发生心跳给eureka服务端，表明它仍然活着
    lease-renewal-interval-in-seconds: 10
    # 表示eureka至上次收到实例的最后一次发出的心跳后，等待下次心跳的超时时间，如果
    # 在这时间范围内，没有收到心跳，则移除该实例
    lease-expiration-duration-in-seconds: 30
spring:
  application:
    # 名字，如果名字相同，Eureka会认为是同一个服务
    name: user-client

```

### 启动类

> UserApplication.java

```java
/**
 * 用户模块-模拟eureka客户端
 *
 * 在Greenwich.SR1版本中，@EnableEurekaClient可有可无
 *
 * @author DHB
 */
@EnableEurekaClient
@SpringBootApplication
public class UserApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }
}

```

这样就把用户模块注册在EurekaServer中，我们启动看看。

![](http://lc-dnchthtq.cn-n1.lcfile.com/2b42cf69260dca7b49a2/SpringCloud%E7%AC%94%E8%AE%B0%28%E4%BA%8C%29-EurekaClient.png)

> user-client就注册上来了，但没有实现任何功能，在下一个笔记再进行记录。

## 代码

<https://gitee.com/GzCodeStudio/spring-cloud-learn>

这是我学习SpringCloud做的测试仓库，在标签的版本迭代会慢慢集成不同的功能学习，有错误的地方欢迎指出。

## CAP定理

这个定理是1998年，加州大学的计算机科学家 Eric Brewer 提出的，分布式系统有三个指标。

```
Consistency ---          一致性
Availability ---         可用性
Partition tolerance ---  分区容错性
```

这三个指标不管是在数据库系统中还是在分布式架构中都是无法同时做到的。

详细看[CAP 定理的含义](http://www.ruanyifeng.com/blog/2018/07/cap.html)

## eureka与zookeeper对比

Zookeeper在设计的时候遵循的是CP原则，即一致性,Zookeeper会出现这样一种情况，当master节点因为网络故障与其他节点失去联系时剩余节点会重新进行leader选举，问题在于，选举leader的时间太长：30~120s，且选举期间整个Zookeeper集群是不可用的，这就导致在选举期间注册服务处于瘫痪状态，在云部署的环境下，因网络环境使Zookeeper集群失去master节点是较大概率发生的事情，虽然服务能够最终恢复，但是漫长的选举时间导致长期的服务注册不可用是不能容忍的。

Eureka在设计的时候遵循的是AP原则，即可用性。Eureka各个节点（服务)是平等的， 没有主从之分，几个节点down掉不会影响正常工作，剩余的节点（服务） 依然可以提供注册与查询服务，而Eureka的客户端在向某个Eureka注册或发现连接失败，则会自动切换到其他节点，也就是说，只要有一台Eureka还在，就能注册可用（保证可用性）， 只不过查询到的信息不是最新的（不保证强一致），除此之外，Eureka还有自我保护机制，如果在15分钟内超过85%节点都没有正常心跳，那么eureka就认为客户端与注册中心出现了网络故障，此时会出现一下情况:

1: Eureka 不再从注册列表中移除因为长时间没有收到心跳而过期的服务。

2：Eureka 仍然能够接收新服务的注册和查询请求，但是不会被同步到其它节点上（即保证当前节点可用）

3： 当网络稳定后，当前实例新的注册信息会被同步到其它节点中
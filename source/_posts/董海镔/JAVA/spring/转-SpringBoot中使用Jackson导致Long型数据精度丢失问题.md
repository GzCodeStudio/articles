---
title: '[转]SpringBoot中使用Jackson导致Long型数据精度丢失问题'
date: 2018-10-08 09:08:45
categories:
  - java
tags:
  - Spring boot
author: 董海镔
---

数据库中有一个bigint类型数据，对应java后台类型为Long型，在某个查询页面中碰到了问题：页面上显示的数据和数据库中的数据不一致。例如数据库中存储的是：1475797674679549851，显示出来却成了1475797674679550000，后面几位全变成了0，精度丢失了。

<!--more-->

# 1. 原因

这是因为Javascript中数字的精度是有限的，bigint类型的的数字超出了Javascript的处理范围。JS 遵循 IEEE 754 规范，采用双精度存储（double precision），占用 64 bit。其结构如图：

{% asset_img 15295704881401.png img %}

各位的含义如下：

- 1位（s） 用来表示符号位
- 11位（e） 用来表示指数
- 52位（f） 表示尾数

尾数位最大是 52 位，因此 JS 中能精准表示的最大整数是 Math.pow(2, 53)，十进制即 9007199254740992。而Bigint类型的有效位数是63位（扣除一位符号位），其最大值为：Math.pow(2,63)。任何大于 9007199254740992 的就可能会丢失精度：

```
9007199254740992     >> 10000000000000...000 // 共计 53 个 0
9007199254740992 + 1 >> 10000000000000...001 // 中间 52 个 0
9007199254740992 + 2 >> 10000000000000...010 // 中间 51 个 0
```

实际上值却是：

```
9007199254740992 + 1 // 丢失
9007199254740992 + 2 // 未丢失
9007199254740992 + 3 // 丢失
9007199254740992 + 4 // 未丢失
```

# 2.解决方法

解决办法就是让Javascript把数字当成字符串进行处理。对Javascript来说，不进行运算，数字和字符串处理起来没有什么区别。当然如果需要进行运算，只能采用其他方法，例如使用JavaScript的一些开源库bignumber之类的处理了。Java进行JSON处理的时候是能够正确处理long型的，只需要将数字转化成字符串就可以了。例如：

```
{
    ...
    "bankcardHash": 1475797674679549851,
    ...
}
```

变为：

```
{
    ...
    "bankcardHash": "1475797674679549851",
    ...
}
```

这样Javascript就可以按照字符串方式处理，不存在数字精度丢失了。在Springboot中处理方法基本上有以下几种：

## 2.1 配置参数write_numbers_as_strings

Jackson有个配置参数`WRITE_NUMBERS_AS_STRINGS`，可以强制将所有数字全部转成字符串输出。其功能介绍为：`Feature that forces all Java numbers to be written as JSON strings.`。使用方法很简单，只需要配置参数即可：

```
spring:
  jackson:
    generator:
      write_numbers_as_strings: true
```

这种方式的优点是使用方便，不需要调整代码；缺点是颗粒度太大，所有的数字都被转成字符串输出了，包括按照timestamp格式输出的时间也是如此。

## 2.2 注解

另一个方式是使用注解`JsonSerialize`：

```
@JsonSerialize(using=ToStringSerializer.class)
private Long bankcardHash;
```

指定了ToStringSerializer进行序列化，将数字编码成字符串格式。这种方式的优点是颗粒度可以很精细；缺点同样是太精细，如果需要调整的字段比较多会比较麻烦。

## 2.3 自定义ObjectMapper

最后想到可以单独根据类型进行设置，只对Long型数据进行处理，转换成字符串，而对其他类型的数字不做处理。Jackson提供了这种支持。方法是对ObjectMapper进行定制。根据SpringBoot的官方帮助（[https://docs.spring.io/spring-boot/docs/current/reference/html/howto-spring-mvc.html#howto-customize-the-jackson-objectmapper），找到一种相对简单的方法，只对ObjectMapper进行定制，而不是完全从头定制，方法如下：](https://docs.spring.io/spring-boot/docs/current/reference/html/howto-spring-mvc.html#howto-customize-the-jackson-objectmapper%EF%BC%89%EF%BC%8C%E6%89%BE%E5%88%B0%E4%B8%80%E7%A7%8D%E7%9B%B8%E5%AF%B9%E7%AE%80%E5%8D%95%E7%9A%84%E6%96%B9%E6%B3%95%EF%BC%8C%E5%8F%AA%E5%AF%B9ObjectMapper%E8%BF%9B%E8%A1%8C%E5%AE%9A%E5%88%B6%EF%BC%8C%E8%80%8C%E4%B8%8D%E6%98%AF%E5%AE%8C%E5%85%A8%E4%BB%8E%E5%A4%B4%E5%AE%9A%E5%88%B6%EF%BC%8C%E6%96%B9%E6%B3%95%E5%A6%82%E4%B8%8B%EF%BC%9A)

```
@Bean("jackson2ObjectMapperBuilderCustomizer")
public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
    Jackson2ObjectMapperBuilderCustomizer customizer = new Jackson2ObjectMapperBuilderCustomizer() {
        @Override
        public void customize(Jackson2ObjectMapperBuilder jacksonObjectMapperBuilder) {
            jacksonObjectMapperBuilder.serializerByType(Long.class, ToStringSerializer.instance)
                    .serializerByType(Long.TYPE, ToStringSerializer.instance);
        }
    };
    return customizer;
}
```

通过定义`Jackson2ObjectMapperBuilderCustomizer`，对`Jackson2ObjectMapperBuilder`对象进行定制，对`Long`型数据进行了定制，使用`ToStringSerializer`来进行序列化。问题终于完美解决。

# 附录、参考资料

- [Customize the Jackson ObjectMapper](https://docs.spring.io/spring-boot/docs/current/reference/html/howto-spring-mvc.html#howto-customize-the-jackson-objectmapper)
- [Java Code Examples for org.springframework.http.converter.json.Jackson2ObjectMapperBuilder](https://www.programcreek.com/java-api-examples/?api=org.springframework.http.converter.json.Jackson2ObjectMapperBuilder)
- [Spring MVC自定义消息转换器(可解决Long类型数据传入前端精度丢失的问题)](https://www.cnblogs.com/Fly-Bob/p/7218006.html)
- [Jackson Annotation Examples](http://www.baeldung.com/jackson-annotations)
- [What Every Computer Scientist Should Know About Floating-Point Arithmetic](https://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html)

# 原文链接

https://orchidflower.gitee.io/2018/06/22/Handling-Bigint-using-Jackson-in-Springboot/
---
title: Spring boot WebMvcConfigurer
date: '2018/09/05 09:21'
updated: '2018/09/05 09:21'
categories:
  - java
tags:
  - Spring boot
author: 董海镔
---

总结WebMvcConfigurer用法

没完成

<!-- more -->

> Spring boot版本：2.0.4

# 使用

> 方法一：启动类implements WebMvcConfigurer

```java
@SpringBootApplication
public class TestApplication implements WebMvcConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }
}
```

> 方法二：新建一个类implements WebMvcConfigurer

```java
@Configuration
public class WebConfig implements WebMvcConfigurer {
    ...
}
```

# 方法总览

| 方法签名                                                     | 解释                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| configurePathMatch(PathMatchConfigurer configurer)           | 配置URL匹配规则                                              |
| configureContentNegotiation(ContentNegotiationConfigurer configurer) | 配置内容协商机制，一个路径可以返回多种数据格式               |
| configureAsyncSupport(AsyncSupportConfigurer configurer)     | 配置异步任务处理                                             |
| configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) | DefaultServletHandlerConfigurer用一个enable()方法，使对静态资源的请求转发到servlet容器上，而不是控制器 |
| addFormatters(FormatterRegistry registry)                    | 配置类型转换，一般用于字符串与对象间的转换                   |
| addInterceptors(InterceptorRegistry registry)                | 配置拦截器                                                   |
| addResourceHandlers(ResourceHandlerRegistry registry)        | 配置静态资源处理策略                                         |
| addCorsMappings(CorsRegistry registry)                       | 配置跨域                                                     |
| addViewControllers(ViewControllerRegistry registry)          | 可以方便的实现一个请求直接映射成视图，而无需书写controller   |
| configureViewResolvers(ViewResolverRegistry registry)        | 配置视图解析器                                               |
| addArgumentResolvers(List< HandlerMethodArgumentResolver> resolvers) | 配置参数解析器，比如添加一个@JsonParam，解析JSON字段注入方法的参数中 |
| addReturnValueHandlers(List< HandlerMethodReturnValueHandler> handlers) | 配置对Controller返回结果进一步处理                           |
| configureMessageConverters(List<HttpMessageConverter<?>> converters) |                                                              |
| configureHandlerExceptionResolvers(List< HandlerExceptionResolver> resolvers) |                                                              |
| extendHandlerExceptionResolvers(List< HandlerExceptionResolver> resolvers) |                                                              |

# 相关文章

## configurePathMatch

## configureContentNegotiation

## configureAsyncSupport

## configureDefaultServletHandling

## addFormatters

[springmvc formatter](https://www.cnblogs.com/yanqin/p/7054164.html)

[Spring Boot：定制type Formatters](https://www.jianshu.com/p/f09052e185de)

## addInterceptors



## addResourceHandlers

[Spring Boot 静态资源处理](https://blog.csdn.net/catoop/article/details/50501706)

## addCorsMappings

[后端接口支持跨域CORS调用](https://www.jianshu.com/p/453afeef49c1)

## configureViewResolvers

[springboot使用addViewController减少控制器代码的编写](https://blog.csdn.net/nerversimply/article/details/79575115)

## configureViewResolvers

[深入Spring:自定义ViewResolver](https://www.jianshu.com/p/b5eaafe2021a)

[Spring4.x官方参考文档中文版——第21章 Web MVC框架(34)](https://blog.csdn.net/hermaeuxmora/article/details/60601775)

## addArgumentResolvers

[通过自定义 @CurrentUser 获取当前登录用户](https://www.jianshu.com/p/01a6a61d9e02)

[spring 自定参数解析器（HandlerMethodArgumentResolver）](https://blog.csdn.net/u010187242/article/details/73647670)
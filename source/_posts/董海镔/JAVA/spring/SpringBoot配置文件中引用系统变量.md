---
title: 'Spring Boot配置文件引用系统环境变量'
date: '2019-05-05 20:40:00'
categories:
  - java
tags:
  - Spring boot
author: 董海镔
---

# 前言

在开发中有些敏感的数据不可以直接写在配置文件中或者是本地测试的一些配置信息，直接修改嘛，会和线上的版本不一致，那就可以通过引用系统变量来动态配置。

<!--more-->

# todo

通过**${系统变量名}**可以引用系统变量

例子：

```yaml
spring:
	datasource:
		name: ${datasource_name}
```

上面的配置，Spring会读取系统变量中的**datasource_name**赋值给name

## 进阶一下

如果忘记配置了系统变量，怎么使用缺省变量呢？

例子：

假设上面的例子中，我忘记配置datasource_name了，想通过一个缺省变量值**test**给它

```yaml
spring:
	datasource:
		name: ${datasource_name:test}
```

在原来的基础上加上**:**，后面是缺省值，但系统变量中找不到datasource_name就会把**test**赋值给name
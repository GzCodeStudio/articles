---
title: '[转]Maven更新父子模块的版本号'
date: 2018-09-20 14:59:59
categories:
  - java
tags:
  - Maven
  - 转载
author: 董海镔
---



设置父子版本号

```sh
$ mvn versions:set -DnewVersion=[版本]
```

<!-- more -->

更新子模块版本号

```sh
$ mvn versions:update-child-modules
```

[原文链接](https://www.cnblogs.com/ilovexiao/p/5663761.html)


---
title: JAVA设计模式-策略模式
date: 2019-04-28 00:00:00
categories:
	- [设计模式]
tags:
	- java
	- 设计模式
author: 董海镔
---



# 概念

策略模式：定义了算法族，分别封装起来，让他们之间可以相互替换，此模式让算法的变化独立于使用算法的客户
<!--more-->

![图1](http://lc-dnchthtq.cn-n1.lcfile.com/6c10d352fba2033b38e3.png)

该模式涉及三个角色：

　　●　**环境(Context)角色：**持有一个Strategy的引用。

　　●　**抽象策略(Strategy)角色：**这是一个抽象角色，通常由一个接口或抽象类实现。此角色给出所有的具体策略类所需的接口。

　　●　**具体策略(ConcreteStrategy)角色：**包装了相关的算法或行为。

策略模式的使用场景：

1）针对同一种问题的多种处理方式、仅仅是因为具体行为有差别时，

2）需要安全的封装多种同一类型的操作时

3）出现同一抽象类有多个子类，而又需要使用if-else或者switch-case来选择具体子类时

# 举个栗子

把生活中的鸭子看作**环境角色**，它们的行动看作**抽象策略角色**，每种动作的实现看作**具体策略角色**。比如，鸭子飞行和叫声的行为，有的可以用翅膀飞行，有不能飞行；有的是呱呱叫，有的是吱吱叫。他们之间的关系图是：

![图2](http://lc-dnchthtq.cn-n1.lcfile.com/8c69c74c8529b0e560c3.png)

鸭子自身不实现飞行和叫声的行为（算法），把这两个行为（算法）托付给了两个接口。每当需要创建出鸭子来的时候，可以决定鸭子这个鸭子的飞行方式和叫声。如果鸭子进化成可以说人话，只需要实现QuackBehavior接口即可，而不用修改鸭子的内部。

# 再一个栗子

在编程中，用户的密码保存到数据库前往往需要对密码加密，可以使用这个模式定义不同的加密算法，随机使用不同的算法加密。[emmm，不知道会不会有人这么做，一般都是加盐加醋]

用户类

```java
public class User {
     /**
     * 加密接口
     */
    private Encryption encryption;

    private String password;

    public void setEncryption(Encryption encryption) {
        this.encryption = encryption;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return encryption.encrypt(password);
    }
}
```

加密接口

```java
public interface Encryption {
    String encrypt(String s);
}
```

两个实现类

```java
public class Md5Encrypt implements Encryption{
    @Override
    public String encrypt(String s) {
        return s + "md5";
    }
}
```

```java
public class SHAEncrypt implements Encryption{
    @Override
    public String encrypt(String s) {
        return s + "sha";
    }
}
```

使用：

```java
public class Main {
    public static void main(String[] args) {
        User user = new User();
        user.setEncryption(new Md5Encrypt());
        user.setPassword("1234");
        System.out.println("伪MD5加密：" + user.getPassword());
        user.setEncryption(new SHAEncrypt());
        System.out.println("伪SHA加密：" + user.getPassword());
    }
}
```

输出：

```
伪MD5加密：1234md5
伪SHA加密：1234sha
```

# 参考来源

[java设计模式--策略模式](https://www.cnblogs.com/wuyudong/p/5924223.html)上文概念内容大部分来源于这文章

Head First 设计模式
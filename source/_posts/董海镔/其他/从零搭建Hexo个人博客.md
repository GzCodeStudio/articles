---
title: 从零搭建Hexo个人博客
date: 2018-08-16 08:42:00
categories: 
	- [其它]
tags: 
	- 博客
author: 董海镔
---

# 前言

Remove

<!-- more-->

# 效果

https://dhbin.cn

# 环境配置

环境: 阿里云Ubuntu16.04

## 安装git

```sh
$ apt update
$ apt install git-core
```

## 安装Nodejs

安装 Node.js 的最佳方式是使用 [nvm](https://github.com/creationix/nvm) 

```sh
$ wget https://raw.github.com/creationix/nvm/master/install.sh
$ sudo chmod +x install.sh
$ ./install.sh
```

执行nvm可能会提示找不到命令，执行下

```sh
$ source ~/.bashrc
```

安装完成后，重启终端并执行下列命令即可安装 Node.js。 

```sh
$ nvm install stable
```

## 安装Hexo

```sh
$ npm install -g hexo-cli
```

## 安装Nginx

```sh
$ apt install nginx
```

如果系统中没有Nginx的源，百度很多教程

# 建站

安装 Hexo 完成后，请执行下列命令，Hexo 将会在指定文件夹中新建所需要的文件。 

```sh
$ hexo init <folder>
$ cd <folder>
$ npm install
```

安装完成后执行

```sh
$ hexo server
```

输出：

INFO  Start processing
INFO  Hexo is running at http://localhost:4000/. Press Ctrl+C to stop.

即可访问

# 配置

## Hexo配置

在Hexo的文档有很详细的配置信息描述，这里主要说下我修改的地方

配置文件:_confg.yml

```yaml
# Site
title: 标题
subtitle: 副标题
description: 描述
keywords: 关键词，用于SEO吧
author: 作者
language: 语言，我在这配置zh-CN
timezone: 时区

# URL
## If your site is put in a subdirectory, set url as 'http://yoursite.com/child' and root as '/child/'
url: 网站
permalink: 文章的 永久链接 格式 我的配置是:year/:month/:day/:id.html # 使用:id的话，执行hexo clean会改变
```

## NexT配置

把NexT下载下来，这里我是下载而不是git clone，因为后来使用git对整个项目托管会有冲突。如果是把项目托管在github的话可以把NexT添加为子模块。

```sh
$ cd <hexo>
$ git submodule add https://github.com/theme-next/hexo-theme-next themes/next
```

修改<hexo>/_config.yml，切换主题为next

```yaml
# Extensions
## Plugins: https://hexo.io/plugins/
## Themes: https://hexo.io/themes/
theme: next
```

### 定制NexT

修改<hexo>/themes/next/_config.yml

```yaml
# Change headers hierarchy on site-subtitle (will be main site description) and on all post/pages titles for better SEO-optimization.
seo: true # seo优化

# 菜单
menu:
  home: / || home
  #about: /about/ || user
  tags: /tags/ || tags
  categories: /categories/ || th
  archives: /archives/ || archive
  #schedule: /schedule/ || calendar
  #sitemap: /sitemap.xml || sitemap
  #commonweal: /404/ || heartbeat

# Enable/Disable menu icons / item badges.
menu_settings:
  icons: true
  badges: true # 开启后菜单显示总数目的badge

# Schemes 样式，NexT提供了四种，我看Mist比较顺眼
#scheme: Muse
scheme: Mist
#scheme: Pisces
#scheme: Gemini

# Social Links.
# Usage: `Key: permalink || icon`
# Key is the link label showing to end users.
# Value before `||` delimeter is the target permalink. 
# Value after `||` delimeter is the name of FontAwesome icon. If icon (with or without delimeter) is not specified, globe icon will be loaded.
# 格式：
# 显示名称: 链接 || 图标
social:
  GitHub: https://github.com/DHBin || github
  Gitee: https://gitee.com/FYMD || code
  
# Sidebar Avatar
# 头像，可以是网上的链接，也可以是本地的
avatar:

# Automatically Excerpt. Not recommend.
# Please use <!-- more --> in the post to control excerpt accurately.
# 这里没有修改，可以使用<!-- more -->来显示摘要
# 如：
# ---
# 我是摘要
# <!-- more -->
# 在首页就仅显示“我是摘要”
auto_excerpt:
  enable: false
  length: 150
  
# Canvas-nest
# Dependencies: https://github.com/theme-next/theme-next-canvas-nest
# 背景动画
canvas_nest: true

# Script Vendors.
# Set a CDN address for the vendor you want to customize.
# For example
#    jquery: https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js
# Be aware that you should use the same version as internal ones to avoid potential problems.
# Please use the https protocol of CDN files when you enable https on your site.
# 使用CDN加速
vendors:
  jquery: https://cdn.bootcss.com/jquery/2.1.3/jquery.min.js
  fastclick: https://cdn.bootcss.com/fastclick/1.0.6/fastclick.min.js
  velocity: https://cdn.bootcss.com/velocity/1.2.1/velocity.min.js
  velocity_ui: https://cdn.bootcss.com/velocity/1.2.1/velocity.ui.min.js
  pace: https://cdn.bootcss.com/pace/1.0.2/pace.min.js
  pace_css: https://cdn.bootcss.com/pace/1.0.2/themes/blue/pace-theme-flash.min.css
  canvas_nest: https://cdn.bootcss.com/canvas-nest.js/1.0.1/canvas-nest.min.js
```

## 部署配置

到这里就基本完成了，把项目部署到服务器。

### PM2

PM2是node进程管理工具 ,我把`hexo server`和自动git pull的脚本分开运行，以cluster模式运行`hexo server`.

>  ecosystem.config.js

```js
module.exports = {
  apps : [{
    name      : '博客',
    script    : '/root/.nvm/versions/node/v10.8.0/bin/hexo',
    instances : 'max',
    args      : ['server', '-p', '8080', '-i', '127.0.0.1'],
    exec_mode : 'cluster',
    watch     : [
        'scaffolds', 'source', 'themes', '_config.yml'
    ],
    ignore_watch : ['node_modules', 'package.json', 'package-lock.json']
  }],
};
```

instances设置了max，看你们的实际情况设置吧

>  strat.sh

```shell
#!/bin/bash
pm2 start ecosystem.config.js
pm2 show 博客
while true
do
    echo '=====git pull======'
    git pull
    echo '=====git pull======'
    sleep 60
done
```

每次更新git push后就会自动更新，每分钟执行一次git pull，也可以使用webhook来实现。

### Nginx

> /etc/nginx/conf.d/blog.conf

```
server {
    server_name xxxx.cn
    listen 80;
    location / {
            proxy_pass  http://localhost:8080;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;
    }
}
```

### 运行

```sh
$ cd <项目的目录>
$ chmod +x start.sh
$ pm2 start start.sh
```

# 其他

## HTTPS证书

可以在https://letsencrypt.org/ 中免费申请一个

## Markdown工具

推荐Typora



可能文中会出现错误和讲述不清的地方，望指出与交流。

# 参考资料

[很好用的markdown工具——typora](https://blog.csdn.net/funnypython/article/details/79306147)

[Hexo官方文档](https://hexo.io/zh-cn/docs/)

[nexT官方文档](http://theme-next.iissnan.com/)
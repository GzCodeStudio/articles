---
title: 超级课程表API
date: '2018/05/30 12:19'
updated: '2018/05/30 12:19'
categories:
  - 其它
tags:
  - API
  - 超级课程表
author: 董海镔
---
感觉现在的超级课程表很多功能过于冗余，想写通过它的API来做一个自己用，结果毕业了
<!-- more -->
版本：4.9.0

平台：安卓

# 总览
url | 备注
---|---|---
http://120.55.151.61/|BaseUrl|
V2/StudentSkip/loginCheckV4.action  | 登陆 
V2/Course/getCourseTableFromServer.action | 获取课程表
V2/Course/getCourseTableBySuperId.action | 通过他人分享获取课程表，如二维码
V2/SMS/getResetPasswordCaptcha.action | 发送修改密码短信
V2/SMS/checkResetPasswordCaptcha.action | 验证短信验证码
V2/StudentSkip/resetPasswordByMobileV4.action | 修改密码

url | 备注
---|---|---
http://112.124.54.19/|BaseUrl|
Score/score/getVerCode.action | 获取教务系统验证码
Score/score/importScoreFromSchool.action | 查询成绩



# 登陆

> POST V2/StudentSkip/loginCheckV4.action

## Body

参数 | 测试值 | 解释
---|---|---
platform|1 | 平台
phoneVersion|19 | 手机版本 安卓为SDK版本
deviceCode|355757010701395 | 设备代码（IMEI）
account|***** | ==加密后账号==
versionNumber|9.4.0 | 超级课程表版本号
password | ***** | ==加密后密码==
updateInfo|false | 
channel|ppMarket | 渠道
phoneBrand|samsung | 手机制造商
phoneModel|SM-G955F | 手机型号



## 备注

> 账号和密码加密算法：aes([context], md5("friday_syllabus"))

附上JAVA实现代码


```java
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.Key;
import java.security.MessageDigest;

public class SuperClassUtil {

    public static String encrypt(String string) {
        String result = null;
        try {
            result = aes(URLEncoder.encode(string, "utf-8"), md5("friday_syllabus"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static String aes(String context, String key) {
        String result = "";
        Key secretKeySpec;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            secretKeySpec = new SecretKeySpec(messageDigest.digest(key.getBytes("utf-8")), "AES");
            Cipher instance = Cipher.getInstance("AES");
            instance.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            result = byteToStr(instance.doFinal(context.getBytes("utf-8")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private static String byteToStr(byte[] bArr) {
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : bArr) {
            String toHexString = Integer.toHexString(b & 255);
            if (toHexString.length() == 1) {
                toHexString = '0' + toHexString;
            }
            stringBuilder.append(toHexString.toUpperCase());
        }
        return stringBuilder.toString();
    }

    private static String md5(String str) {
        int i = 0;
        String str2 = null;
        char[] cArr = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            char[] cArr2 = new char[32];
            int i2 = 0;
            while (i < 16) {
                byte b = digest[i];
                int i3 = i2 + 1;
                cArr2[i2] = cArr[(b >>> 4) & 15];
                i2 = i3 + 1;
                cArr2[i3] = cArr[b & 15];
                i++;
            }
            str2 = new String(cArr2).toUpperCase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str2;
    }
}
```


# 获取课程表

> POST V2/Course/getCourseTableFromServer.action

## Header


key | value
---|---
JSESSIONID | 登陆后获取
SERVERID | 登陆后获取

## Body

参数 | 测试值 | 解释
---|---|---
platform | 1 | 
phoneVersion | 19
term | 2 | 第几学期[1、2]
beginYear | 2017 | 年份
versionNumber | 9.4.0
channel | ppMarket
phoneBrand | samsung
phoneModel | SM-G955F



# 通过他人分享获取课程表，如二维码

> POST V2/Course/getCourseTableBySuperId.action 

他人分享的二维码中包含了*term*、*beginYear*、*superId*

## Header


key | value
---|---
JSESSIONID | 登陆后获取
SERVERID | 登陆后获取

## Body

参数 | 测试值 | 解释
---|---|---
platform | 1 
phoneVersion | 19
versionNumber | 9.4.0
channel | ppMarket
phoneBrand | samsung
phoneModel | SM-G955F
term | 2 | 学期
beginYear | 2017 | 年份
superId | 23110790 | 成绩课程表用户id


# 发送修改密码短信

> POST V2/SMS/getResetPasswordCaptcha.action

## Body

参数 | 测试值 | 解释
---|---|---
retry | 0
platform | 1
phoneVersion | 19
versionNumber | 9.4.0
areaCode | null
channel | ppMarket
phoneBrand | samsung
phoneModel | SM-G955F
m | ***** | 手机号码
mm | ***** | aes(手机号码)


# 验证短信验证码

> POST V2/SMS/checkResetPasswordCaptcha.action

## Body

参数 | 测试值 | 解释
---|---|---
platform | 1
m | ***** | 手机号码
phoneVersion | 19
captcha | 253059 | 验证码
versionNumber | 9.4.0
areaCode | null
channel | ppMarket
phoneBrand | samsung
phoneModel | SM-G955F


# 修改密码

> POST V2/StudentSkip/resetPasswordByMobileV4.action 

## Body
参数 | 测试值 | 解释
---|---|---
platform | 1
mobileNumber | ***** | 手机号码
phoneVersion | 19
captcha | 253059 | 验证码
newPassword | ***** | 加密后的密码，参考登陆
versionNumber | 9.4.0
areaCode | null
channel | ppMarket
phoneBrand | samsung
phoneModel | SM-G955F


# 获取教务系统验证码

> GET Score/score/getVerCode.action

## query

参数 | 测试值 | 解释
---|---|---
identity | 6ADF482EEF5A9B5D0CC47C7E864AC3BC | 登陆后返回值中获取
schoolIdentity | 307CD6AA7D26520033EF2429279E6079 |  学校ID、登陆后返回值中获取
ts | 1527672631 | Unix时间戳


# 获取成绩

> POST Score/score/importScoreFromSchool.action 

## Header

key | value
---|---
JSESSIONID | 获取教务系统验证码Response中获取

## Body
参数 | 测试值 | 解释
---|---|--- 
v_c | 6ukr | 验证码
t_m | 2 | 
schoolIdentity | 307CD6AA7D26520033EF2429279E6079 | 学校ID，登陆后获取
s_n | ***** | 学校教务系统账号 
s_Id | 0 |
p_d | ***** | 学校教务系统密码
identity | 6ADF482EEF5A9B5D0CC47C7E864AC3BC | Id，登陆后获取
b_y | 2017 | 年份

---
title: socket笔记
date: 2019-04-30 16:44:59
tags:
  - socket
categories:
  - C++
author: 董海镔
---

最近在学习redis，了解到redis仅是单线程的程序，却有非常高的性能。在想了解其的io模型时，卡在了socket上，刚好想写个程序同步本站的文章，所以就用cpp写了个简单解析http协议的程序

<!--more-->

# 代码

```c++
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>


using namespace std;

int get_line(int, char *, int);


/**********************************************************************/
/* Get a line from a socket, whether the line ends in a newline,
 * carriage return, or a CRLF combination.  Terminates the string read
 * with a null character.  If no newline indicator is found before the
 * end of the buffer, the string is terminated with a null.  If any of
 * the above three line terminators is read, the last character of the
 * string will be a linefeed and the string will be terminated with a
 * null character.
 * Parameters: the socket descriptor
 *             the buffer to save the data in
 *             the size of the buffer
 * Returns: the number of bytes stored (excluding null)
 * https://github.com/EZLippi/Tinyhttpd/blob/master/httpd.c
 * */
/**********************************************************************/
int get_line(int socket, char *buffer, int size) {
    int i = 0;
    char c = '\0';
    int n;

    while ((i < size - 1) && (c != '\n')) {
        /*
         * 当没有数据时这里会阻塞
         * */
        n = recv(socket, &c, 1, 0);
        /* DEBUG printf("%02X\n", c); */
        if (n > 0) {
            if (c == '\r') {
                n = recv(socket, &c, 1, MSG_PEEK);
                /* DEBUG printf("%02X\n", c); */
                if ((n > 0) && (c == '\n'))
                    recv(socket, &c, 1, 0);
                else
                    c = '\n';
            }
            buffer[i] = c;
            i++;
        } else
            c = '\n';
    }
    buffer[i] = '\0';
    return (i);
}


/**********************************************************************/
/*
 * 功能：监听端口，接收http协议，当url与参数2匹配时执行git pull
 *
 * 注：写这个程序主要是为了学习socket和http协议，进而了解tomcat的io模型
 *
 * 参数1： 端口号
 * 参数2： url地址
 * */
/**********************************************************************/
int main(int argc,char **argv) {
    int server;

    /*
     * socket函数的方法签名
     * int socket (int __family, int __type, int __protocol)
     * __family     定义协议族
     * __type       定义数据传输方式/套接字类型
     * __protocol   定义传输协议
     *
     * 返回值是文件描述符
     *
     * */
    server = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (server < 0) {
        cout << "=> 连接失败" << endl;
    }

    /*
    * 位于netinet/in.h
    * 将套接字和IP、端口判断
    *
    * */
    struct sockaddr_in server_addr{};
    // 使用IPv4
    server_addr.sin_family = AF_INET;
    // 端口
    server_addr.sin_port = htons(atol(argv[1]));
    // IP地址
    server_addr.sin_addr.s_addr = htons(INADDR_ANY);

    /*
     * struct sockaddr* 位于socket.h
     * bind()签名
     * int bind (int, const struct sockaddr *__my_addr, socklen_t __addrlen);
     * 第一个参数：socket文件描述符
     * 第二个参数：struct sockaddr*
     * 第三个参数：struct sockaddr* 长度
     * */
    int bind_error;
    if ((bind_error = bind(server, (struct sockaddr *) &server_addr, sizeof(server_addr))) < 0) {
        cout << "=> 绑定socket异常:" << bind_error << endl;
        close(server);
        exit(1);
    }


    // 设置监听
    listen(server, SOMAXCONN);

    struct sockaddr_in client_addr{};
    socklen_t client_addr_len = sizeof(client_addr);
    int client;
    int buffer_size = 1024;
    char buffer[buffer_size];
    char sendStr[] = "HTTP/1.1 200 OK\r\n\r\nok";
    while (true) {
        client = accept(server, (struct sockaddr *) &client_addr, &client_addr_len);

        if (client < 0) {
            cout << "=> accept 错误" << endl;
            exit(1);
        }
        int len;
        int content_length = -1;
        char method[255];
        char url[255];
        /*
         * i： 数组偏移量
         * j： 行数偏移量
         * */
        size_t i, j;
        i = 0;
        j = 0;

        /* 读取报文第一行 */
        len = get_line(client, buffer, buffer_size);
        /*================== 解析请求方法 ==================*/
        while (!isspace(buffer[i]) && i < sizeof(method) - 1) {
            method[i] = buffer[i];
            i++;
        }
        j = i;
        method[i] = '\0';
        cout << "请求方法:" << method << endl;
        /*================== 解析请求方法 ==================*/


        /*================== 解析url ==================*/

        i = 0;
        /* 跳过空格 */
        while (isspace(buffer[j]) && (j < len))
            j++;

        while (!isspace(buffer[j]) && (i < sizeof(url) - 1) && (j < len)) {
            url[i] = buffer[j];
            i++;
            j++;
        }
        url[i] = '\0';
        cout << "URL：" << url << endl;
        /*================== 解析url ==================*/

        while ((len = get_line(client, buffer, buffer_size))) {
            if (buffer[0] != '\n') {
                cout << buffer;
                /* 设置Content-Length:的长度，截断数组取Content-Length的值 */
                buffer[15] = '\0';
                if (strcasecmp(buffer, "Content-Length:") == 0) {
                    char *stop;
                    content_length = strtol(&(buffer[16]), &stop, 10);
                }
                memset(buffer, 0, len);
            } else {
                break;
            }
        }
        /*================== 解析请求数据 ==================*/
        // 置零，下面读取请求数据
        len = 0;
        if (content_length > 0) {
            do {
                int tmp = recv(client, buffer, buffer_size, 0);
                len += tmp;
                cout << buffer << endl;
                memset(buffer, 0, tmp);
            } while (len != content_length);
        }
        /*================== 解析请求数据 ==================*/
        if (strcmp(argv[2], url) == 0) {
            system("git pull");
        }
        send(client, sendStr, sizeof(sendStr), 0);
        close(client);
    }
}
```

# 资料

[Socket入门资料](http://c.biancheng.net/view/2123.html)

[Linux_socket编程入门（C++）](https://www.jianshu.com/p/676506ad2057)

[C 实现一个简易的Http服务器](https://www.cnblogs.com/life2refuel/p/5277111.html)

[Tinyhttpd源码](https://github.com/EZLippi/Tinyhttpd)


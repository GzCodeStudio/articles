---
title: '解决Nginx出现forbidden (13: Permission denied)'
date: 2018-09-20 08:55:30
categories:
  - 服务器
tags:
  - Nginx
author: 董海镔
---



Nginx访问目录时报Permission denied权限不足

```sh
xxxxx is forbidden (13: Permission denied), client: xxxx
```

<!-- more -->

# 解决方法

1. 打开Nginx配置文件

   ```sh
   $ vim /etc/nginx/nginx.conf
   ```

2. 修改user

   ```nginx
   #user www-data;
   user root;
   worker_processes auto;
   pid /run/nginx.pid;
   ```

3. 重载Nginx

   ```sh
   $ nginx -s reload
   ```
